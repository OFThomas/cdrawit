#include <dirent.h>	// DIR
#include <sys/stat.h>	// stat struct used in DIR

// #include <threads.h>

// comment this def to go full screen
// #define cdDEBUG

#define CDIMPLEMENTATION
#include "cdrawit.h"

// subroutines
void cdColorTest(cdScreen *s);
void cdPlotTest(cdScreen *s, float (*f)(float), int numXpoints,	int height,
		cdColor_t color, char * title);

void cdColorTest(cdScreen *s)
{
	while(1) {
		cdScreenPrint(s);
		for (int i = 0; i < s->height; i++) {
			cdScreenSet(s, i, i, "#", cdDefault);
		}

		for (int i = 0; i < s->height; i++) {
			cdScreenSet(s, i, s->height - i - 1,
					"\xE2\x98\xA0", cdDefault);
		}

		for (int i = 0; i < s->width; i++) {
			cdScreenSet(s, i, 0, "\xE2\x98\xA0", cdRed);
		}

		for (int i = 0; i < s->width; i++) {
			char* c = cdScreenGet(s, i, 0);
			cdScreenSet(s, i, 1, c, cdGreen);
		}
		cdScreenPrint(s);
		break;
	}
}

void cdPlotTest(cdScreen *s, float (*f)(float), int numXpoints,	int height,
		cdColor_t color, char *title)
{
	cdPlot plot;
	cdPlotTitle(&plot, title);
	cdPlotEvaluateFunc(&plot, -2.0f * M_PI, 2.0f * M_PI,
			numXpoints, f);
	// cdPlotXvals(p, xvals);
	// cdPlotDraw(s, &plot, xpos, ypos, height, color);
	cdAddPlot(s, &plot, height, title, color);
}

void brailleTest(void)
{
	int i;
	for (i = 0; i < 192; i++) {
		if ( (i) % 8 == 0 ) {
			printf("\n");
		}
		brailleCode(i);
	}
}

void readProc(char *output, int outputSize, const char *filename)
{
	FILE *fp = fopen(filename, "r");
	if (!fp) {
		printf("Error opening %s\n", filename);
		return;
	}
	const int BUFSIZE = 128;
	char buf[BUFSIZE];
	memset(buf, '\0', BUFSIZE);
	// clear the output
	memset(output, '\0', outputSize);
	int offset = 0;
	while (	fgets(buf, BUFSIZE, fp) ) {
		int lineSize = strlen(buf);
		// for (int i = 0; i < lineSize; i++) {
		// 	if (buf[i] == '\n') {
		// 		buf[i] = '\0';
		// 	}
		// }
		if (offset + lineSize >= outputSize) {
			fclose(fp);
			return;
		}
		offset += sprintf(output + offset, "%s", buf);
	}
	fclose(fp);
}

#define CDWIDGETHISTORY 20

void cpuLoad(cdScreen *s)
{
	// open /proc/loadavg
	// open /proc/stat
	int dataSize = CDWIDGETHISTORY;
	static float loadData[5][CDWIDGETHISTORY];

	const int BUFSIZE = 2048;
	char loadavg[BUFSIZE], stat[BUFSIZE], net[BUFSIZE];
	memset(loadavg, '\0', BUFSIZE);
	memset(stat, '\0', BUFSIZE);
	memset(net, '\0', BUFSIZE);
	readProc(loadavg, BUFSIZE, "/proc/loadavg");
	readProc(stat, BUFSIZE, "/proc/stat");
	readProc(net, BUFSIZE, "/proc/net/dev");

	// printf("before load avg:\n%s\n", loadavg);
	cdRemoveLastEndLine(loadavg);
	cdAddStringBox(s, "Load avg", loadavg, cdGreen);
	// cdAddStringBox(s, "Stats", stat, cdGreen);

	// parseLoadAvg(loadavg, loadData);
	char *tok = strtok(loadavg, " ,");
	int i = 0;
	while (tok) {
		float val = strtof(tok, NULL);
		for (int j = 0; j < CDWIDGETHISTORY -1; j++) {
			loadData[i][j] = loadData[i][j+1];
		}

		// printf("tok: %s, Val: %f\n", tok, val);
		loadData[i][CDWIDGETHISTORY-1] = val;
		i++;
		tok = strtok(NULL, " ,");
	}

	// printfVectorf(loadData[0], dataSize);
	cdPlot p;
	cdPlotSet(&p, loadData[0], dataSize);
	cdAddPlot(s, &p, 5, "LoadAvg", cdWhite);

	// printf("\nLoadavg\n%s\n", loadavg);
	// printf("\nstat\n%s\n", stat);
}

// https://benjaminwuethrich.dev/2015-03-07-sorting-strings-in-c-with-qsort.html
int cmpstr(const void *a, const void *b)
{
	const char *aa = (const char *)a;
	const char *bb = (const char *)b;
	return strcmp(aa, bb);
}

// Most from
// https://web.eecs.utk.edu/~jplank/plank/classes/cs360/360/notes/Stat/lecture.html
void printDir(char *output, int *outputSize, char *directory)
{
	struct stat statbuf;
	int exists;

	if (!output) {
		printf("Error output buffer not valid! in printDir\n");
		return;
	}


	// chdir(directory);
	DIR *dir = opendir(directory);
	struct dirent *de;
	if (!dir) {
		printf("Error opening: %s\n", directory);
		return;
	}

	int numEntries = 0;
	char files[100][64];
	int maxWidth = 0;
	for (de = readdir(dir); de != NULL; de = readdir(dir)) {
		// printf("de->d_name: %s\n", de->d_name);
		// lstat is for symlinks but is not c99 compliant
		exists = stat(de->d_name, &statbuf);
		if (exists < 0) {
			printf("%s not found\n", de->d_name);
			return;
		}
		strcpy(files[numEntries], de->d_name);
		numEntries++;
		maxWidth = cdMAX(maxWidth, (int)strlen(de->d_name));
	}
	closedir(dir);

	qsort(files, numEntries, sizeof(*files), cmpstr);
	for (int i = 0; i < numEntries; i++) {
		// lstat is for symlinks but is not c99 compliant
		exists = stat(files[i], &statbuf);
		// printf("%*s %ld\n", -maxWidth, files[i], statbuf.st_size);
		*outputSize -= (maxWidth + 1 + 8);
		if (*outputSize > 0) {
			output += sprintf(output, "%*s %8ld\n",
					-maxWidth, files[i], statbuf.st_size);
		}
	}
}

void pwd(cdScreen *s)
{
	char *dir = "./";
	int outputSize = 1024 * 1024;
	char *output = malloc(outputSize * sizeof(*output));
	printDir(output, &outputSize, dir);
	cdRemoveLastEndLine(output);
	cdAddStringBox(s, "ls", output, cdDefault);

	// printf("%s\n", output);
	free(output);
}

void test()
{
	int RUNLOOP = 1;

	while (RUNLOOP) {
		cdScreen *s = cdScreenNew();

		cdPlotTest(s, quadratic, 30, 7,
				cdRed, "Plot 1");
		cdPlotTest(s, cubic, 20, 9,
				cdGreen, "Some other plot, idk");
		cdPlotTest(s, cosf, 16, 3,
				cdYellow, "Cos cos cos");
		cdPlotTest(s, sinf, 16, 3, cdCyan,
				"Sin");

		cdAddStringBox(s, "Blue", "Some Blue\n text",
				cdBlue);

		// cdScreenSetStringBox(s, -13, 5, 12, 3,
		// 		"Hi, this is some test text", cdMagenta);

		cdAddStringBox(s, "Test text",
				"Hi, this is some test test",
				cdMagenta);

		{
			cdPlot plot;
			cdPlotEvaluateFunc(&plot, -2.0f * M_PI,
					2.0f * M_PI, 30,
					cosf);
			cdAddPlot(s, &plot, 7, "cos",
					cdWhite);
		}


		{
			cdPlot plot;
			cdPlotEvaluateFunc(&plot, -2.0f * M_PI,
					2.0f * M_PI, 30,
					quadratic);
			cdAddPlot(s, &plot, 7, "quadratic",
					cdWhite);
		}

		// cdUserInputs();
		cpuLoad(s);
		pwd(s);

		cdScreenPrint(s);

		char c = getch()[0];
		printf("char: %c\n", c);

		if (c == '\t') {
			printf("Tab\n");
		}

		if (c == 'q') {
			printf("Exiting\n");
			RUNLOOP = 0;
		}

		cdScreenFree(s);
	}
	return;
}
// typedef struct cdRubiksSquares {
// 	cdColor_t col[6][3][3];
// } cdRubiksSquares;
//
typedef cdColor_t cdRubiksSquares[6][3][3];

void cdRubiksRotFace(cdRubiksSquares sq, char axis, int direction)
{
	//	     0  1  2
	//           3  4  5
	//	     6  7  8 
	//
	// 9  10 11  18 19 20  27 28 29  36 37 38
	// 12 13 14  21 22 23  30 31 32  39 40 41
	// 15 16 17  24 25 26  33 34 35  42 43 44
	//
	// 	     45 46 47
	// 	     48 49 50
	// 	     51 52 53
	//
	// The different faces are indexed by multiples of 9 into the Square
	// struct. For an arbritrary reason we choose to display the top, left
	// and middle faces, so elements {0, 26} of the square

	// direction = +1 for clockwise, -1 for anti-clockwise
	// face is index [0, 5]
	//   0
	// 1 2 3 4
	//   5

	// There are 6 moves
	// U, D, R, L, F, B

	// 3 Rotations (of the whole cube)
	// X, Y, Z

	// X maps 
	// so from initial config this is rotating around 2 & 4
	// 0 -> 3 -> 5 -> 1
	// and spin face 2, 4

	cdColor_t tmp[3][3];
	int faces[4];

	switch (axis) {
	case 'x':
		faces[0] = 0;
		faces[1] = 1;
		faces[2] = 5;
		faces[3] = 3;
		break;
	case 'y':
		faces[0] = 1;
		faces[1] = 2;
		faces[2] = 3;
		faces[3] = 4;
		break;
	case 'z':
		faces[0] = 0;
		faces[1] = 4;
		faces[2] = 5;
		faces[3] = 2;
		break;
	}

	for (int i = 0; i < 4; i++) {
		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 3; y++) {
				if (i == 0) {
					tmp[x][y] = sq[faces[0]][x][y];
				}

				if (i < 3) {
					sq[faces[i]][x][y] = sq[faces[i+1]][x][y];
				} else {
					sq[faces[3]][x][y] = tmp[x][y];
				}
			}
		}
	}
}


void cdRubiksMove(cdRubiksSquares sq, char input, int direction)
{
	//	     0  1  2
	//           3  4  5
	//	     6  7  8 
	//
	// 9  10 11  18 19 20  27 28 29  36 37 38
	// 12 13 14  21 22 23  30 31 32  39 40 41
	// 15 16 17  24 25 26  33 34 35  42 43 44
	//
	// 	     45 46 47
	// 	     48 49 50
	// 	     51 52 53
	//
	// The different faces are indexed by multiples of 9 into the Square
	// struct. For an arbritrary reason we choose to display the top, left
	// and middle faces, so elements {0, 26} of the square

	// direction = +1 for clockwise, -1 for anti-clockwise
	// face is index [0, 5]
	//   0
	// 1 2 3 4
	//   5

	// There are 6 moves
	// U, D, R, L, F, B


	cdColor_t tmp[3][3];
	int *faces;

	switch (input) {
	case 'u':
		// rotate top 
		faces = (int[4]){1, 2, 3, 4};
		for (int i = 0; i < 4; i++) {
			for (int x = 0; x < 3; x++) {
				const int y = 2;
				if (i == 0) {
					tmp[x][y] = sq[faces[0]][x][y];
				}

				if (i < 3) {
					sq[faces[i]][x][y] = sq[faces[i+1]][x][y];
				} else {
					sq[faces[3]][x][y] = tmp[x][y];
				}
			}
		}
		break;
	case 'd':
		faces = (int[4]){4, 3, 2, 1};
		for (int i = 0; i < 4; i++) {
			for (int x = 0; x < 3; x++) {
				const int y = 0;
				if (i == 0) {
					tmp[x][y] = sq[faces[0]][x][y];
				}

				if (i < 3) {
					sq[faces[i]][x][y] = sq[faces[i+1]][x][y];
				} else {
					sq[faces[3]][x][y] = tmp[x][y];
				}
			}
		}
		break;
	case 'r':
		faces = (int[4]){1, 5, 3, 0};
		for (int i = 0; i < 4; i++) {
			const int x = 2;
			for (int y = 0; y < 3; y++) {
				if (i == 0) {
					tmp[x][y] = sq[faces[0]][x][y];
				}

				if (i < 3) {
					sq[faces[i]][x][y] = sq[faces[i+1]][x][y];
				} else {
					sq[faces[3]][x][y] = tmp[x][y];
				}
			}
		}
		break;
	case 'l':
		faces = (int[4]){0, 3, 5, 1};
		for (int i = 0; i < 4; i++) {
			const int x = 0;
			for (int y = 0; y < 3; y++) {
				if (i == 0) {
					tmp[x][y] = sq[faces[0]][x][y];
				}

				if (i < 3) {
					sq[faces[i]][x][y] = sq[faces[i+1]][x][y];
				} else {
					sq[faces[3]][x][y] = tmp[x][y];
				}
			}
		}
		break;
	case 'f':
		faces = (int[4]){4, 5, 2, 0};
		for (int i = 0; i < 4; i++) {
			for (int x = 0; x < 3; x++) {
				if (i == 0) {
					tmp[x][0] = sq[faces[0]][x][0];
				}

				if (i == 1) {
					sq[faces[i]][x][0] = sq[faces[i+1]][x][0];
				} else if (i == 0 || i == 2) {
					sq[faces[i]][0][x] = sq[faces[i+1]][0][x];
				} else {
					sq[faces[3]][x][0] = tmp[x][0];
				}
			}
		}		break;
	case 'b':
		faces = (int[4]){0, 4, 5, 2};
		break;
	}

}

void cdRubiks(cdScreen *s, char *input)
{
	cdWin win = {.box.w = 34, .box.h = 15};
	cdBox *b = &win.box;
	if (!cdFreeCoords(s, b)) {
		return;
	}
	cdScreenBorder(s, *b, "Cube", cdMagenta);

	// isometric points
	//      Z  Z  Z 
	//    Z  Z  Z Y
	//  Z  Z  Z Y Y
	//  x x x Y Y Y
	//  x x x Y Y
	//  x x x Y
	//       Z
	//     Z   Z
	//   Z   Z   Z  
	//  x  Z   Z  Y
	//  x x  Z  Y Y
	//  x x x Y Y Y
	//    x x Y Y
	//      x Y
	//

	static cdColor_t fCol[6] = {cdWhite, cdRed, cdBlue, cdOrange, cdGreen, cdYellow};

	//	     0  1  2
	//           3  4  5
	//	     6  7  8 
	//
	// 9  10 11  18 19 20  27 28 29  36 37 38
	// 12 13 14  21 22 23  30 31 32  39 40 41
	// 15 16 17  24 25 26  33 34 35  42 43 44
	//
	// 	     45 46 47
	// 	     48 49 50
	// 	     51 52 53
	//
	// The different faces are indexed by multiples of 9 into the Square
	// struct. For an arbritrary reason we choose to display the top, left
	// and middle faces, so elements {0, 26} of the square
	static cdRubiksSquares sq;
	static int Setup = 1;

	if (Setup) {

		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 3; j++) {
				for (int k = 0; k < 3; k++) {
					sq[i][j][k] = fCol[i];
				}
			}
		}
		Setup = 0;
	}

	b->x++;
	b->y++;

	for (int i = 0; i < 6; i++) {
		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 3; y++) {
				int xv, yv;
				const int yOffset = 7;

				switch (i) {
				case 0: // white
					xv = 2*x + 1 + 2*y; 
					yv = 3 + y + yOffset;
					break;
				case 1: // red
					xv = 2*x;
					yv = y + yOffset;
					break;
				case 2: // blue
					xv = 2*x + 3 * 2;
					yv = y + x + yOffset;
					break;
				case 3: // orange
					xv = 2*x + 3 * 2;
					yv = y + x;
					break;
				case 4: // green
					xv = 2*x;
					yv = y;
					break;
				case 5: // yellow
					xv = 2*x + 1 + 2*y; 
					yv = 3 + y;
					break;

				}
				cdScreenSet(s, b->x + xv, b->y + yv, cdFullBlock, sq[i][x][y]);
			}
		}
	}

	
	b->x += 20;
	for (int y = 0; y < 3; y++) {
		for (int x = -y; x <= y; x += 2) {
			cdScreenSet(s, b->x + 5 + 2*x , b->y + 3 + y, cdFullBlock, fCol[0]);
			cdScreenSet(s, b->x + 5 + 2*x , b->y + 3 -y + 4, cdFullBlock, fCol[0]);
		}

		for (int x = 0; x < 6; x += 2) {
			cdScreenSet(s, b->x + x, b->y + y - x/2 + 2, cdFullBlock, fCol[1]);
			cdScreenSet(s, b->x + 6 + x, b->y + y+x/2, cdFullBlock, fCol[2]);
		}
	}

	// b->x -= 20;
	// cdWindowAddString(s, &win, "x, y, z to rotate ", cdMagenta);
	b->x -= 6;
	b->y += 12;
	cdScreenSetString(s, *b, "x, y, z to rotate", cdMagenta);

	cdColor_t tmp[3][3];
	int faces[4] = {0, 1, 5, 3};
	switch (input[0]) {
		case 'x': // rotate around right
		case 'y': // rotate around top
		case 'z': // rotate around front
			cdRubiksRotFace(sq, input[0], 1);
			break;
		case 'u':
		case 'd':
		case 'r':
		case 'l':
		case 'f':
		case 'b':
			cdRubiksMove(sq, input[0], 1);
			break;
		case 'a':
			Setup = 1;
			break;
	}
}

typedef char cdSudokuGrid[9][9];

int cdSudokuCheckValid(cdSudokuGrid su, char notValid[9][9])
{
	// check the board
	// first check for duplicates in rows
	for (int r = 0; r < 9; r += 3) {
		int count[3][9] = {0};
		for (int cell = r; cell < r+3; cell++) {
			for (int j = 0; j < 3; j++) {
				for (int i = 3*j; i < 3*(j+1); i++) {
					if (su[cell][i] == '\0') {
						continue;
					}
					int idx = su[cell][i] - '1';
					count[j][idx]++;
				}
			}
		}

		for (int cell = r; cell < r+3; cell++) {
			for (int j = 0; j < 3; j++) {
				for (int i = 3*j; i < 3*(j+1); i++) {
					if (su[cell][i] == '\0') {
						continue;
					}
					int idx = su[cell][i] - '1';
					notValid[cell][i] += count[j][idx] - 1;
				}
			}
		}
	}

	// check for duplicates in cols
	for (int c = 0; c < 3; c++) {
		int count[3][9] = {0};
		for (int cell = c; cell < 9; cell += 3) {
			for (int j = 0; j < 3; j++) {
				for (int i = j; i < 9; i += 3) {
					if (su[cell][i] == '\0') {
						continue;
					}
					int idx = su[cell][i] - '1';
					count[j][idx]++;
				}
			}
		}

		for (int cell = c; cell < 9; cell += 3) {
			for (int j = 0; j < 3; j++) {
				for (int i = j; i < 9; i += 3) {
					if (su[cell][i] == '\0') {
						continue;
					}
					int idx = su[cell][i] - '1';
					notValid[cell][i] += count[j][idx] - 1;
				}
			}
		}
	}

	// check for duplicates in cells
	for (int cell = 0; cell < 9; cell++) {
		int count[9] = {0};
		for (int i = 0; i < 9; i++) {
			if (su[cell][i] == '\0') {
				continue;
			}
			int idx = su[cell][i] - '1';
			count[idx]++;
		}
		for (int i = 0; i < 9; i++) {
			if (su[cell][i] == '\0') {
				continue;
			}
			int idx = su[cell][i] - '1';
			notValid[cell][i] += count[idx] - 1;
		}
	}

	int sum = 0;
	for (int cell = 0; cell < 9; cell++) {
		for (int i = 0; i < 9; i++) {
			sum += notValid[cell][i];
		}
	}
	return !sum;
}

void cdSudokuAddNumber(cdSudokuGrid su) {
	// look for an empty square, insert a number, check if valid, if so
	// return else try all the numbers 1-9
	// pick random cell and space
	int cell = cdRandMax(9);
	int pos = cdRandMax(9);

	char num = '1';

	char current = su[cell][pos];
	if (current == '\0') {
		su[cell][pos] = num;
		char notValid[9][9] = {0};
		while (!cdSudokuCheckValid(su, notValid) && num <= '9') {
			su[cell][pos] = num;
			num++;
		}
	}
}

int cdSudokuBacktrack(cdSudokuGrid start, cdSudokuGrid su, int idx) {
	// look for an empty square, insert a number, check if valid, if so
	// return else try all the numbers 1-9
	// pick random cell and space

	int cell = idx / 9;
	int pos = idx % 9;
	if (start[cell][pos] != '\0') {
		return idx + 1;
	}

	if (su[cell][pos] == '\0') {
		su[cell][pos] = '1';
	} else {
		su[cell][pos]++;
	}

	while (su[cell][pos] <= '9') {
		char notValid[9][9] = {0};
		if (cdSudokuCheckValid(su, notValid)) {
			return idx+1;
		}
		su[cell][pos]++;
	}
	// if we made it here none of the values are correct, so set to '\0'
	// and decrement the idx 
	su[cell][pos] = '\0';
	// so if the previous is a start (i.e. fixed value we need to skip over
	// it and keep going backwards
	do {
		idx--;
		if (idx < 0) {
			idx = 0;
			return idx;
		}
		cell = idx / 9;
		pos = idx % 9;
	} while (start[cell][pos] != '\0');

	return idx;
}


void cdSudoku(cdScreen *s, char *input)
{
	// grid of 9x9 characters
	cdColor_t color = cdMagenta;
	const int w = 10;
	const int h = 4;
	cdWin win = {.box.w = 3*w+3, .box.h = 3*h+5}; // don't ask why it is +3
	cdBox *b = &win.box;
	if (!cdFreeCoords(s, b)) {
		return;
	}
	cdScreenBorder(s, *b, "Sudoku", color);

	cdScreenSetString(s, (cdBox){.x = b->x+1, .y=b->y+b->h-2, .w =b->w-3, .h=b->h},
			"wasd to move, 1-9, x/0 to clear", color); 
	cdScreenSetString(s, (cdBox){.x = b->x+6, .y=b->y+b->h-3, .w =b->w-3, .h=b->h},
			"r reset, g to solve", color); 

	// row, col
	static cdSudokuGrid su;
	static cdSudokuGrid start; // used as starting point for solver

	static int cursorCell = 0;
	static int cursorNum = 0;

	static int Setup = 0;
	if (Setup) {
		memset(su, '\0', sizeof(su));
		memset(start, '\0', sizeof(su));
		Setup = 0;
	}

	b->x++;
	b->y++;

	// draw borders
	// All this code is to draw the stupid box around the numbers
	for (int x = 0; x <= 3*w; x++) {
		for (int y = 0; y <= 3*h; y++) {
			int xtru = (x % w) == 0;
			int ytru = (y % h) == 0;
			int xc = b->x + x;
			int yc = b->y + y;
			if (xtru) {
				cdScreenSet(s, xc, yc, cdVLine, cdDefault);
			}
			if (ytru) {
				cdScreenSet(s, xc, yc, cdHLine, cdDefault);
			}

			if (xtru && ytru) {
				cdScreenSet(s, xc, yc, cdCross, cdDefault);
			}

			// I swear there will be an easier way to do this, but
			// today is not the day I figure it out
			if (y == 0) {
				if (x == 0) {
					cdScreenSet(s, xc, yc, cdCurveNE, cdDefault);
				} else if (x == 3*w) {
					cdScreenSet(s, xc, yc, cdCurveNW, cdDefault);
				} else if (xtru) {
					cdScreenSet(s, xc, yc, cdHUp, cdDefault);
				}
			} else if (y == 3*h) {
				if (x == 0) {
					cdScreenSet(s, xc, yc, cdCurveSE, cdDefault);
				} else if (x == 3*w) {
					cdScreenSet(s, xc, yc, cdCurveSW, cdDefault);
				} else if (xtru) {
					cdScreenSet(s, xc, yc, cdHDown, cdDefault);
				}
			} else if (y == h || y == 2*h) {
				if (x == 0) {
					cdScreenSet(s, xc, yc, cdRDown, cdDefault);
				} else if (x == 3*w) {
					cdScreenSet(s, xc, yc, cdLDown, cdDefault);
				}
			}
		}
	}
	
	// write a cell
	for (int cell = 0; cell < 9; cell++) {
		const int xoffset = (cell % 3) * w;
		const int yoffset = (cell / 3) * h;
		const int x = b->x + xoffset;
		const int y = b->y + yoffset;
		// numbers in cell
		for (int i = 0; i < 9; i++) {
			const int x = b->x + xoffset + 2 + (i % 3) * 3;
			const int y = b->y + yoffset + 1 + i / 3;
			if (su[cell][i] != '\0') {
				cdScreenSetChar(s, x, y, su[cell][i]);
				cdScreenSetColor(s, x, y, cdDefault);
			}
			
			if (cursorCell == cell && cursorNum == i) {
				cdColor_t color = cdScreenGetColor(s, x, y);
				// cdAddHexBox(s, "color", color, color);
				// cdSetfgColor(&color, 0x008000);
				cdSetbgColor(&color, 0x404040);
				cdScreenSetColor(s, x, y, color);
			}
		}
	}

	// check valid
	char notValidPos[9][9] = {0};
	if (!cdSudokuCheckValid(su, notValidPos)) {
		for (int cell = 0; cell < 9; cell++) {
			for (int i = 0; i < 9; i++) {
				if (notValidPos[cell][i]) {
					int xoffset = (cell % 3) * w;
					int yoffset = (cell / 3) * h;
					int x = b->x + xoffset + 2 + (i % 3) * 3;
					int y = b->y + yoffset + 1 + i / 3;
					cdColor_t color = cdScreenGetColor(s, x, y);
					cdSetfgColor(&color, cdRed);
					cdScreenSetColor(s, x, y, color);
				}
			}
		}
	}

	static int Solver = 0;
	static int SolverSetup = 0;
	static int SolverIdx = 0;

	if (Solver) {
		const int maxNumMovesPerFrame = 250;
		for (int i = 0; i < maxNumMovesPerFrame; i++) {
			if (SolverIdx < 81) {
				SolverIdx = cdSudokuBacktrack(start, su, SolverIdx);
				// draw sudoku
			}
		}
	}

	char c = input[0];
	if (input[0] == '\033' && input[1] == '[') { // arrow key escapes
		c = input[2];
	}

	switch (c) {
	case 'r':
		Setup = 1;
		Solver = 0;
		break;
	case 'x':
	case '0':
		start[cursorCell][cursorNum] = '\0';
		memcpy(su, start, 9*9*(sizeof(**su)));
		SolverIdx = 0;
		break;
	case 'a': 
	case 'D': // arrow key left
		if (cursorNum % 3 == 0) { // left column
			// move to right column and go to prev cell
			cursorNum += 2;
			if (cursorCell % 3 == 0){ cursorCell += 2; }
			else			{ cursorCell--; }
		} else {
			cursorNum--;
		}
		break;
	case 'd':
	case 'C': // arrow key right
		if ((cursorNum-2) % 3 == 0) { // Right column
			// move to left column and go to next cell
			cursorNum -= 2; 
			if ((cursorCell-2) % 3 == 0){ cursorCell -= 2; }
			else			{ cursorCell++; }
		} else {
			cursorNum++;
		}
		break;
	case 'w':
	case 'A': // arrow key up
		if (cursorNum >= 6) { // top row
			// move to left column and go to next cell
			cursorNum -= 6; 
			if (cursorCell >= 6)	{ cursorCell -= 6; }
			else			{ cursorCell += 3; }
		} else {
			cursorNum += 3;
		}
		break;
	case 's':
	case 'B': // arrow key down
		if (cursorNum <= 2) { // bottom row
			// move down row and go to next cell
			cursorNum += 6; 
			if (cursorCell <= 2)	{ cursorCell += 6; }
			else			{ cursorCell -= 3; }
		} else {
			cursorNum -= 3;
		}
		break;
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
		start[cursorCell][cursorNum] = input[0];
		memcpy(su, start, 9*9*(sizeof(**su)));
		SolverIdx = 0;
		break;
	case 'g':
		Solver ^= 1;
		// memset(su, '\0', sizeof(su));
		memcpy(su, start, 9*9*(sizeof(**su)));
		SolverIdx = 0;
		break;
	}
}

int benchmark(int targetFps, unsigned long long numCycles)
{
	int RUNLOOP = 1;

	cdScreen *s = cdScreenNew();
	const int screenFrameRate = targetFps;

	while (RUNLOOP) {
		cdScreenNewFrame(s);

		char c = getch()[0];

		char inputbuf[8];
		inputbuf[7] = '\0';
		memcpy(inputbuf, cdINPUTBUF, 4);
		
		cdAddStringBox(s, "Input", inputbuf, cdWhite);

		if (c == '\t') {
			printf("Tab\n");
		}

		if (c == 'q') {
			printf("Exiting\n");
			RUNLOOP = 0;
		}

		struct timespec timeNow = cdGetTimeNow();

		cdAddFloatBox(s, "FrameTime (ms)", s->frameTimePassed/1e3,
				cdGreen);
		cdAddFloatBox(s, "FPS", s->currentFPS, cdCyan);
                cdAddIntBox(s, "Time (us)",
                            timeNow.tv_sec * CDNANOSECONDS + timeNow.tv_nsec,
                            cdRed);
                cdAddIntBox(s, "Counter", s->counter, cdRed);

		int plotHistory = 15;
		static cdPlot frameTimePlot, fpsPlot;
		if (cdTimePassedms(s, 100)) {
			cdPlotPushValue(&frameTimePlot, s->frameTimePassed /1e3,
					plotHistory);
			cdPlotPushValue(&fpsPlot, s->currentFPS, plotHistory);
		}

		cdAddPlot(s, &frameTimePlot, 8, "FrameTime (ms)", cdGreen);
		cdAddPlot(s, &fpsPlot, 8, "fps", cdCyan);

		cdMenu(s, cdINPUTBUF);

		cdSudoku(s, cdINPUTBUF);
		cdRubiks(s, cdINPUTBUF);


		char buf[256];
		int offset = sprintf(buf, "StartTime: %llu, Runtime: %llu\n",
				cdTimespecToInt(s->startTime),
				cdTimespecToInt(s->runTime));
		cdFormatTime(buf + offset, s->runTime);
		cdAddStringBox(s, "Time stats", buf, cdOrange);


		cdScreenRender(s, screenFrameRate);
		if (s->counter > numCycles) {
			break;
		}
	}
	char buf[256];
	cdFormatTime(buf, s->runTime);
	printf("%s\n", buf);
	cdScreenFree(s);
	return 0;
}

int main()
{
	return benchmark(144, 20000);
}
