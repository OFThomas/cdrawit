#ifndef CDRAWDEFC_H
#define CDRAWDEFC_H

#ifndef CDIMPLEMENTATION // color implementation
#define CDIMPLEMENTATION
#endif

// #define cdDEBUG

#ifndef M_PI
#    define M_PI 3.14159265358979323846
#endif

#endif /* end of include guard CDRAWDEFC_H */

#ifndef CDRAWCOLORS_H
#define CDRAWCOLORS_H

typedef unsigned long long cdColor_t;

#define cdDefault	0xf0f0f0
#define cdRed		0xff0000
#define cdGreen		0x00c020
#define cdBlue		0x0030ff
#define cdWhite		0xffffff
#define cdMagenta	0xff00ff
#define cdCyan		0x00ffff
#define cdYellow	0xd0ff00
#define cdOrange	0xe09000
#define cdGrey		0x808080

#define cdReset		"\033[0m"

#define cdHLine 	"\u2500"
#define cdHLineDots	"\u2500"
#define cdVLine 	"\u2502"
#define cdVLineDots	"\u2502"

#define cdCurveSE 	"\u256d"
#define cdCurveSW 	"\u256e"
#define cdCurveNW 	"\u256f"
#define cdCurveNE 	"\u2570"

#define cdRDown		"\u251c"
#define cdLDown		"\u2524"
#define cdHDown		"\u252c"
#define cdHUp		"\u2534"
#define cdCross		"\u253c"

#define cdFullBlock	"\u2588"
#define cdUHalfBlock	"\u2580"
#define cdLHalfBlock	"\u2584"

void cdSetfgColor(cdColor_t *col, cdColor_t rgb);
void cdSetbgColor(cdColor_t *col, cdColor_t rgb);
void cdSetfgbgColor(cdColor_t col);

#ifdef CDIMPLEMENTATION // color implementation
#include <stdio.h>

void cdSetfgColor(cdColor_t *col, cdColor_t rgb)
{
	// and with 0xff to make sure we are [0,255]
	// clear bits [23:0]
	*col &= ~((1 << 24) - 1);
	*col |= (rgb & 0xffffff);
}

void cdSetbgColor(cdColor_t *col, cdColor_t rgb)
{
	// and with 0xff to make sure we are [0,255]
	// clear bits [48:24]
	*col &= ~(((1ull << 24) - 1) << 24);
	*col |= (rgb & 0xffffff) << 24;
}

void cdSetfgbgColor(cdColor_t col)
{
	// order is
	// bgB{47:40}, bgG{39:32}, bgR{31:24}
	// fgB{23:16}, fgG{15:8}, fgR{7:0}
	if (col == cdDefault) {
		// printf(cdReset);
		return;
	}
	unsigned char c[6];
	for (int i = 0; i < 6; i++) {
		c[i] = (col >> (8*i)) & 0xff;
	}

	printf("\033[38;2;%d;%d;%d;48;2;%d;%d;%dm%s",
			c[2], c[1], c[0], c[5], c[4], c[3], "");
}

#endif // CDIMPLEMENTATION


#endif /* end of include guard CDRAWCOLORS_H */

//----------------------- cdScreen ---------------------------------

#ifndef cdSCREEN_H
#define cdSCREEN_H

#ifdef cdDEBUG
#define cdLOGEMPTY() {\
	fprintf(s->logFile,"%s: %d, %s\n", __FILE__, __LINE__, __func__);\
}
#define cdLOG(x) {\
	fprintf(s->logFile,"%s: %d, %s ", __FILE__, __LINE__, __func__);\
	fprintf(s->logFile, x);\
	fprintf(s->logFile, "\n");\
}
#else
#define cdLOGEMPTY() ((void)0)
#define cdLOG(x) ((void)0)
#endif // cdDEBUG

#define printfcdBox(box) "Box x: %d, y: %d, w: %d, h: %d", box.x, box.y, box.w, box.h

// -------------------- utils --------------------------------
#include <stdio.h>	// for FILE
#include <time.h>	// for clock_gettime
#define cdMIN(a,b) (((a)<(b))?(a):(b))
#define cdMAX(a,b) (((a)>(b))?(a):(b))

// User input
extern char cdINPUTBUF[4];
int kbhit();
void setupGetch(void);
char *getch(void);

void cdInputTest(char *Buffer, int MAXBUFFER);
void printfBuffer(char *Buffer);

// debug
void printfVectorf(float *v, int n);
void printfVectori(int *v, int n);

void generateBrailleStr(char *buf, int idx);
void brailleCode(int idx);

float quadratic(float x);
float cubic(float x);

void cdRemoveLastEndLine(char *str);

#define CDNANOSECONDS 1000000000

struct timespec cdGetTimeNow();
void cdFormatTime(char *buf, struct timespec ts);
struct timespec cdDiffTime(struct timespec t1, struct timespec t2);
float cdDiffTimef(struct timespec t1, struct timespec t2);
unsigned long long cdTimespecToInt(struct timespec t1);

int cdRandMax(int n);

// ------------------------ screen ------------------------------------
#define cdHIDECURSOR()	printf("\033[?25l")
#define cdSHOWCURSOR()	printf("\033[?25h")
#define cdCLEAR()	printf("\033[?25l" "\033[H\033[J" "\033[?25h")
#define cdGOTOxy(x,y)	printf("\033[%d;%dH", (y), (x))
#define cdSETCURSOR(x,y)printf("\033[%d;%dH", (y), (x))

// #include "cdrawit.h"
// unicode we are using is 3 bytes, need +1 for '\0'
#define cdSTRSIZE 4 
#define cdSCREENLOGSIZE 2048

// Screen Status values
#define SCREENBLANK	0
#define SCREENBACKGROUND 1
#define SCREENWRITTEN	2
#define SCREENOUTOFBOUNDS 3
#define SCREENDELTA	4

#define cdNUMFRAMEHISTORY 32

// DrawScreen
typedef struct cdScreen {
	unsigned char idx, fitVerticalOrder, ForceREDRAW;
	int width, height;
	float currentFPS;
	unsigned long long counter;
	double frameTimes[cdNUMFRAMEHISTORY], avgFrameTime, maxFrameTime;
	double frameTimePassed;
	struct timespec startTime, runTime;
	struct timespec frameStartTime;
	char *status;
	// The array of 2 is for the two screen buffers (alternates each frame)
	// They are pointers because the screen can be resized
	// buf is an array of cdSTRSIZE because we support unicode
	// array of size 2 of pointer to array of cdSTRSIZE char
	char (*buf[2])[cdSTRSIZE];
	// array of size 2 of pointers to cdColor_t
	cdColor_t *colors[2];

	int showCursor;
	int cursorX, cursorY;
	FILE *logFile;
} cdScreen;

typedef struct cdBox {
	int x, y, w, h;
} cdBox;

char *sprintfcdBox(char *buf, cdBox box);

// Screen should have a ctor, free
// set, get
// print

int checkFits(cdScreen *s, int xStart, int yStart, int w, int h);
int cdFreeCoords(cdScreen *s, cdBox *box);
int cdFreeCoordsRegion(cdScreen *s, cdBox *innerBox, cdBox outerBox);

// used for screen
int CHECKWITHIN(cdScreen *s, int x, int y);

// Screen functions
cdScreen *cdScreenNew();
void cdScreenFree(cdScreen *s);
void cdScreenPrint(cdScreen *s);

// Set
void cdScreenSet(cdScreen *s, int x, int y, char *c, cdColor_t color);
void cdScreenSetChar(cdScreen *s, int x, int y, char c);
void cdScreenSetColor(cdScreen *s, int x, int y, cdColor_t color);
void cdScreenSetAll(cdScreen *, char *c);
void cdScreenSetPattern(cdScreen *, char *c);
void cdScreenSetColorAll(cdScreen *, cdColor_t color);
void cdScreenSetBox(cdScreen *s, cdBox b, char *symbols, cdColor_t color);

void cdScreenNewFrame(cdScreen *s);
void cdScreenRender(cdScreen *s, float frameRate_in);

void cdAddStringBox(cdScreen *s, char *title, char *str, cdColor_t color);
void cdAddIntBox(cdScreen *s, char *title, long long value, cdColor_t color);
void cdAddHexBox(cdScreen *s, char *title, unsigned long long value,
		cdColor_t color);
void cdAddFloatBox(cdScreen *s, char *title, float value, cdColor_t color);
// Get
unsigned long long cdScreenGetCounter(cdScreen *s);
int cdScreenIsFrameMod(cdScreen *s, int n);
int cdTimePassedms(cdScreen *s, int n);
char *cdScreenGet(cdScreen *s, int x, int y);
cdColor_t cdScreenGetColor(cdScreen *s, int x, int y);
int calcStringBoxSize(cdScreen *s, cdBox *box, char *str);

// Status
char cdScreenGetStatus(cdScreen *s, int x, int y);
void cdScreenSetStatus(cdScreen *s, int x, int y, char STATUS);
void cdScreenPrintStatus(cdScreen *s);

void cdScreenBorder(cdScreen *s, cdBox box, char *title, cdColor_t color);
void cdScreenClear(cdScreen *s);

int cdGetStringLineNumber(char *str, int width);
int cdScreenSetString(cdScreen *s, cdBox box, char *str, cdColor_t color);
void cdScreenSetStringBox(cdScreen *s, cdBox box, char *str, cdColor_t color);

#ifdef CDIMPLEMENTATION // cdScreen implementation

#include <string.h>	// memset, memcpy
#include <stdio.h>	// printf
#include <stdlib.h>	// malloc
#include <sys/ioctl.h>	// ioctl to get terminal dimensions
#include <sys/time.h>	// gettimeofday
#include <time.h>	// clock_gettime
#include <unistd.h>	// usleep
#include <termios.h>	// termios
#include <assert.h>

// --------------------- utils ------------------------------
int cdRandMax(int n)
{
	return rand() % n;
}

struct timespec cdGetTimeNow()
{
	// https://man7.org/linux/man-pages/man2/clock_gettime.2.html
	struct timespec  ts;
	if (clock_gettime(CLOCK_MONOTONIC, &ts) == -1) {
		perror("clock_gettime");
		exit(EXIT_FAILURE);
	}
	return ts;
}

void cdFormatTime(char *buf, struct timespec ts)
{
	unsigned long long timeS = ts.tv_sec;
	unsigned long long timeNS = ts.tv_nsec;
	int days, hours, mins, secs, msecs, usecs, nsecs;

	const unsigned long long secAmt = 1;
	const unsigned long long minAmt = 60 * secAmt;
	const unsigned long long hourAmt = 60 * minAmt;
	const unsigned long long dayAmt = 24 * hourAmt;

	days = (timeS / dayAmt);
	hours = (timeS % dayAmt) / hourAmt;
	mins = (timeS % hourAmt) / minAmt;
	secs = (timeS % minAmt) / secAmt;

	const unsigned long long nsecAmt = 1;
	const unsigned long long usecAmt = 1000;
	const unsigned long long msecAmt = 1000 * usecAmt;

	msecs = (timeNS / msecAmt);
	usecs = (timeNS % msecAmt) / usecAmt;
	nsecs = (timeNS % usecAmt) / nsecAmt;

	sprintf(buf, "day: %d, hours: %d, mins: %d, secs: %d, msecs: %d, "
			"usecs: %d, nsec: %d", days, hours, mins, secs, msecs,
			usecs, nsecs);
}

char *sprintfcdBox(char *buf, cdBox box)
{
	sprintf(buf, "x: %d, y: %d, w: %d, h: %d", box.x, box.y, box.w, box.h);
	return buf;
}

void cdPutcharN(char c, int n)
{
	for (int i = 0; i < n; i++) {
		putchar(c);
	}
}

void generateBrailleStr(char *buf, int idx)
{
	if (!buf) {
		return;
	}
	// see
	// https://www.cogsci.ed.ac.uk/~richard/utf-8.cgi?input=%E2%A0%80&mode=char
	// And
	// https://en.wikipedia.org/wiki/Braille_Patterns

	int upper = ((idx >> 6) & 0xf);
	int lower = (idx & 0x3f);
	// printf("upper: %x, lower: %x\n", upper, lower);
	sprintf(buf, "\xe2%c%c", 0xa0 + upper, 0x80 + lower);
}

void brailleCode(int idx)
{
	char buf[8];
	generateBrailleStr(buf, idx);
	printf("i (%3d, %1s) ", idx, buf);
}

// From, https://www.flipcode.com/archives/_kbhit_for_Linux.shtml
int kbhit()
{
	static const int STDIN = 0;
	static char initialized = 0;

	if (!initialized) {
		// Use termios to turn off line buffering
		struct termios term;
		tcgetattr(STDIN, &term);
		term.c_lflag &= ~ICANON;
		term.c_lflag &= ~ECHO;
		tcsetattr(STDIN, TCSANOW, &term);
		setbuf(stdin, NULL);
		initialized = !initialized;
	}

	int bytesWaiting;
	ioctl(STDIN, FIONREAD, &bytesWaiting);
	return bytesWaiting;
}

void setupGetch()
{
	static int keyboardIsSetup = 0;
	if (keyboardIsSetup) {
		return;
	} else {
		keyboardIsSetup = 1;
	}
	struct termios old = {0};
	fflush(stdout);
	if(tcgetattr(0, &old) < 0) {
		perror("tcsetattr()");
	}
	old.c_lflag &= ~ICANON;
	old.c_lflag &= ~ECHO;
	old.c_cc[VMIN] = 1;
	old.c_cc[VTIME] = 0;
	if(tcsetattr(0, TCSANOW, &old) < 0) {
		perror("tcsetattr ICANON");
	}
	old.c_lflag |= ICANON;
	old.c_lflag |= ECHO;
	if(tcsetattr(0, TCSADRAIN, &old) < 0) {
		perror("tcsetattr ~ICANON");
	}
	// printf("%c\n", buf);
}

// From, https://stackoverflow.com/a/16361724
char cdINPUTBUF[4];
char *getch(void)
{
	setupGetch();
	cdINPUTBUF[0] = '\0';
	cdINPUTBUF[1] = '\0';
	cdINPUTBUF[2] = '\0';
	// char buf = '\0';
	if (kbhit()) {
		if(read(0, cdINPUTBUF, 3) < 0) {
			perror("read()");
		}
	}
	// printf("%c\n", buf);
	return cdINPUTBUF;
}

void cdInputTest(char *Buffer, int MAXBUFFER)
{
	setupGetch();
	static int i = 0;
	while(1) {
		// printf(".\n");
		// sleep(1);
		if(kbhit()) {
			Buffer[i] = getch()[0];

			// printf("c = %c\n", Buffer[i]);
			// fflush(stdout);
			i++;
			if (i >= MAXBUFFER) {
				i = 0;
			}
		}
	}
}

void printfBuffer(char *Buffer)
{
	while(1) {
		printf("buf=%s\n", Buffer);
		fflush(stdout);
	}
}

float quadratic(float x)
{
	return x * x;
}

float cubic(float x)
{
	 return x * x * x;
}

void printfVectorf(float *v, int n)
{
	printf("[");
	for (int i = 0; i < n; i++) {
		printf("%6.2f", v[i]);
		if (i != n - 1) {
			printf(", ");
		}
		if ( (i+1) % 8 == 0 && i != 0 ) {
			printf("\n ");
		}
	}
	printf("]\n");
}

void printfVectori(int *v, int n)
{
	printf("[");
	for (int i = 0; i < n; i++) {
		printf("%8d", v[i]);
		if (i != n - 1) {
			printf(", ");
		}
		if ( (i+1) % 8 == 0 && i != 0 ) {
			printf("\n ");
		}
	}
	printf("]\n");
}

void cdRemoveLastEndLine(char *str)
{
	const int len = strlen(str);
	for (int i = len - 1; i >= 0; i++) {
		if (str[i] == '\n') {
			str[i] = '\0';
			return;
		}
	}
}

// ---------------------- Screen -----------------------------------------

int CHECKWITHIN(cdScreen *s, int x, int y)
{
	// return 1;
	int Xover = ((x) >= (s->width));
	int Yover = ((y) >= (s->height));
	char buf[128];
	char *sp = buf;
	if (Xover || Yover)
	{
		cdScreenPrint(s);
		sp += sprintf(sp, "x: %d, y: %d\n", x, y);
		sp += sprintf(sp, "Error not within bounds!\n");

		if (Xover && !Yover) {
			sp += sprintf(sp, "X overflow! Last 5 characters are: ");
			for (int i = s->width - 5; i < 4; i++) {
				sp += sprintf(sp, "%s", cdScreenGet(s, i, y));
			}
		}
		if (Yover && !Xover) {
			sp += sprintf(sp, "Y overflow! Last 5 characters are: ");
			for (int i = s->height - 5; i < 4; i++) {
				sp += sprintf(sp, "%s", cdScreenGet(s, x, i));
			}
		}

		printf("%s\n", buf);
#ifdef cdDEBUG
		fprintf(s->logFile, "%s\n", buf);
		fflush(s->logFile);
		fclose(s->logFile);
#endif // cdDEBUG
		exit(1);
		return 0;
	}
	return 1;
}

void cdScreenSwapFrameBuffer(cdScreen *s)
{
	s->idx = s->idx ^ 1;
}

char cdScreenGetStatus(cdScreen *s, int x, int y)
{
	if (!CHECKWITHIN(s, x, y)) {
		return SCREENOUTOFBOUNDS;
	}
	return s->status[y * s->width + x];
}

void cdScreenSetStatus(cdScreen *s, int x, int y, char STATUS)
{
	if (!CHECKWITHIN(s, x, y)) {
		return;
	}
	s->status[y * s->width + x] = STATUS;
}

void cdScreenPrintStatus(cdScreen *s)
{
	for (int y = 0; y < s->height; y++) {
		for (int x = 0; x < s->width; x++) {
			int stat = cdScreenGetStatus(s, x, y);
			if (stat == SCREENBLANK || stat == SCREENBACKGROUND) {
				cdScreenSet(s, x, y, cdFullBlock, cdGrey);
                        } else if (stat == SCREENWRITTEN) {
				cdScreenSet(s, x, y, cdFullBlock, cdGreen);
                        } else {
				cdScreenSet(s, x, y, cdFullBlock, cdRed);
                        }
		}
	}
	cdScreenPrint(s);
	cdSetfgbgColor(cdGrey);
	printf("\t" cdFullBlock " is free" cdReset ", ");
	cdSetfgbgColor(cdGreen);
	printf(cdFullBlock " is used" cdReset ", ");
	cdSetfgbgColor(cdRed);
	printf(cdFullBlock " is out of bounds\n" cdReset);
}

void cdScreenClear(cdScreen *s)
{
	for (int y = 0; y < s->height; y++) {
		for (int x = 0; x < s->width; x++) {
			int idx = y * s->width + x;
			// printf("x: %d, y: %d, idx: %d\n", x, y, idx);
			s->buf[s->idx][idx][0] = ' ';
			s->buf[s->idx][idx][1] = '\0';
			s->colors[s->idx][idx] = cdDefault;
			s->status[idx] = SCREENBLANK;
		}
	}
	cdScreenBorder(s, (cdBox){.x=0, .y=0, .w=s->width, .h=s->height},
			"", cdDefault);
}

cdScreen *cdScreenNew()
{
	// cdScreen *s = malloc(sizeof(cdScreen));
	// Using malloc makes valgrind report a uninitialized variable, calloc
	// resolves this issue. I do not understand why :/
	// https://www.reddit.com/r/cs50/comments/xs4jnl/valgrind_uninitialised_value_was_created_by_a/
	cdScreen *s = calloc(1, sizeof(cdScreen));
	if (s == NULL) {
		printf("Error malloc screen\n");
		exit(1);
	}
	s->startTime = cdGetTimeNow();

	struct winsize cdWinSize; // from ioctl
	ioctl(0, TIOCGWINSZ, &cdWinSize);
	s->width = cdWinSize.ws_col - 1;
	s->height = cdWinSize.ws_row - 1;

#ifdef cdDEBUG
	s->width = cdWinSize.ws_col - 3; // debug
	s->height = cdWinSize.ws_row-12;
#endif

	const int dim = s->width * s->height;
	s->status = malloc(dim * sizeof(*s->status));
	for (int i = 0; i < 2; i++) {
		s->buf[i] = malloc(dim * sizeof(*s->buf[i]));
		s->colors[i] = malloc(dim * sizeof(s->colors[i]));
		if (s->buf[i] == NULL || s->colors[i] == NULL
				|| s->status == NULL) {
			printf("%s", "Error malloc screen->buf, "
					"screen->colors or screen->status\n");
			exit(1);
		}
	}

	cdCLEAR();
	s->counter = 0;
	s->fitVerticalOrder = 0;
	s->frameTimePassed = 0;
	s->currentFPS = 0;
	s->avgFrameTime = 100;
	s->maxFrameTime = 100;
	s->cursorX = 0;
	s->cursorY = 0;
	s->showCursor = 1;

	cdScreenClear(s);
	s->ForceREDRAW = 1;
	cdScreenPrint(s);
	s->ForceREDRAW = 0;

	cdScreenNewFrame(s);
	return s;
}

void cdScreenFree(cdScreen *s)
{
	for (int i = 0; i < 2; i++) { // 2 for number of screen buffers
		free(s->buf[i]);
		free(s->colors[i]);
	}
	free(s->status);
	free(s);
	cdSHOWCURSOR();
}

int checkFits(cdScreen *s, int xStart, int yStart, int w, int h)
{
	int x, y;
	int xEnd = xStart + w;
	int yEnd = yStart + h;
	for (y = yStart; y < yEnd; y++) {
		for (x = xStart; x < xEnd; x++) {
			// Warning this is much faster bug dangerous as we
			// yolo the status (no bounds checking)
			// This will break if we change how the status is
			// accessed or if the indexing changes.
			// for me this gives 4x speed up not using
			// screengetstatus
			int idx = y * s->width + x;
			int status = s->status[idx];
			// int status = cdScreenGetStatus(s, x, y);
			// printf("(%d, %d): %d\n", x, y, status);
			if (status != SCREENBLANK) {
				// printf("returning\n");
				return 0;
			}
		}
	}
#ifdef cdDEBUG
	fprintf(s->logFile, "Checkfits\n\txStart: %d, w: %d, xEnd: %d, ",
			xStart, w, xEnd);
	fprintf(s->logFile, "\tyStart: %d, h: %d, yEnd: %d\n", yStart, h,yEnd);
#endif // cdDEBUG

	return 1;
}

// takes a width and height and sets xpos, ypos to the origin (bottom left)
// of a place on the screen that can fit the object.
// returns 0 for failure, 1 for success.
int cdFreeCoords(cdScreen *s, cdBox *box)
{
	cdBox wholeScreen = {.x = 0, .y = 0, .w = s->width, .h = s->height};
	return cdFreeCoordsRegion(s, box, wholeScreen);
}

int cdFreeCoordsRegion(cdScreen *s, cdBox *innerBox, cdBox outerBox)
{
	int xStart = outerBox.x;
	int yStart = outerBox.y;
	int xEnd = xStart + outerBox.w - 1 - innerBox->w;
	int yEnd = yStart + outerBox.h - 1 - innerBox->h;

#ifdef cdDEBUG
	fprintf(s->logFile, "cdFreeCoordsRegion\n");
	char boxBuf[64];
	fprintf(s->logFile, "\tinnerBox: %s\n", sprintfcdBox(boxBuf, *innerBox));
	fprintf(s->logFile, "\touterBox: %s\n", sprintfcdBox(boxBuf, outerBox));
#endif // cdDEBUG
	assert((xStart >= 0) && (xEnd < s->width));
	assert((yStart >= 0) && (yEnd < s->height));
	assert((xStart <= xEnd) && (yStart <= yEnd));

	if (s->fitVerticalOrder) {
		// we want to fit box of w x h on the screen start at 0,0 scan
		// along the x axis, if we have ' ' characters we are good to
		// write to that part of the screen
		for (int x = xStart; x < xEnd; x++) {
			for (int y = yEnd; y >= yStart; y--) {
				// check that those starting coordinates fit
				if (checkFits(s, x, y, innerBox->w, innerBox->h)) {
					innerBox->x = x;
					innerBox->y = y;
#ifdef cdDEBUG
					fprintf(s->logFile,
							"Found free coords\n");
					char boxBuf[64];
					fprintf(s->logFile,
							"\tinnerBox: %s\n",
							sprintfcdBox(boxBuf,
								*innerBox));
#endif // cdDEBUG
					return 1;
				}
			}
		}
	} else { // fit horizontal order
		 // we want to fit box of w x h on the screen start at 0,0 scan
		 // along the x axis, if we have ' ' characters we are good to
		 // write to that part of the screen
		for (int y = yEnd; y >= yStart; y--) {
			for (int x = xStart; x < xEnd; x++) {
				// check that those starting coordinates fit
				if (checkFits(s, x, y,
							innerBox->w,
							innerBox->h)) {
					innerBox->x = x;
					innerBox->y = y;
#ifdef cdDEBUG
					fprintf(s->logFile,
							"Found free coords\n");
					char boxBuf[64];
					fprintf(s->logFile,
							"\tinnerBox: %s\n",
							sprintfcdBox(boxBuf,
								*innerBox));
#endif // cdDEBUG
					return 1;
				}
			}
		}
	}
#ifdef cdDEBUG
	fprintf(s->logFile, "Error getting free space\n");
#endif // cdDEBUG
	return 0;
}

void cdScreenBorder(cdScreen *s, cdBox box, char *title, cdColor_t color)
{
	int xpos = box.x;
	int ypos = box.y;
	int width = box.w;
	int height = box.h;
	if ( xpos < 0 ) {
		xpos = s->width + xpos;
	}
	if ( ypos < 0 ) {
		ypos = s->height + ypos;
	}

	const int xInterval = 5;
	const int yInterval = 2;
	for (int y = ypos; y < ypos + height; y++) {
		for (int x = xpos; x < xpos + width; x++) {
			const int isX = ((x % xInterval) == 0);
			const int isY = ((y % yInterval) == 0);
			cdColor_t bgCol = 0x404040;
			if (isX && isY) {
				cdScreenSet(s, x, y, cdCross, bgCol);
				cdScreenSetStatus(s, x, y, SCREENBLANK);
			} else if (isX) {
				cdScreenSet(s, x, y, cdVLine, bgCol);
				cdScreenSetStatus(s, x, y, SCREENBLANK);
			} else if (isY) {
				cdScreenSet(s, x, y, cdHLine, bgCol);
				cdScreenSetStatus(s, x, y, SCREENBLANK);
			}
		}
	}

	for (int i = xpos; i < xpos + width; i++) {
		cdScreenSet(s, i, ypos, cdHLine, color);
		cdScreenSet(s, i, ypos + height - 1, cdHLine, color);
	}
	for (int i = ypos; i < ypos + height; i++) {
		cdScreenSet(s, xpos, i, cdVLine, color);
		cdScreenSet(s, xpos + width - 1, i, cdVLine, color);
	}

	cdScreenSet(s, xpos, ypos, cdCurveNE, color);
	cdScreenSet(s, xpos, ypos + height - 1, cdCurveSE, color);
	cdScreenSet(s, xpos + width - 1, ypos, cdCurveNW, color);
	cdScreenSet(s, xpos + width - 1, ypos + height - 1,
			cdCurveSW, color);


	if (title && title[0] != '\0') {
		// calc how big the title needs to be and adjust the border
		// accordingly
		// we have <--- w ---> and want to fit some text t in the mid
		//           <t>
		// // find mid of <-w-> then subtract half of <t> to get the x
		// position of t so that it is centered
		// int wMid = borderWidth / 2 - len / 2;
		int reducedWidth = (7 * width) / 8;
		// original start + 1/4 of the width
		int reducedXStart = xpos + (width - reducedWidth);

		cdBox box = {.x = reducedXStart, .y = ypos + height - 1,
			.w = reducedWidth};
		cdScreenSetString(s, box, title, color);
	}
}

int cdFastBufCmp(cdScreen *s, int idx)
{
	return !((s->buf[0][idx][0] == s->buf[1][idx][0]) &&
		(s->buf[0][idx][1] == s->buf[1][idx][1]) &&
		(s->buf[0][idx][2] == s->buf[1][idx][2]));
}

void cdScreenPrint(cdScreen *s)
{
	int x, y;

	// I don't think changing from printf to sprintf made any difference.
	cdHIDECURSOR();
	cdGOTOxy(0,0);
	for (y = s->height - 1; y >= 0; y--) {
		for (x = 0; x < s->width; x++) {
			// if (!CHECKWITHIN(s, x, y)) {
			// 	continue;
			// }
			int idx = y * s->width + x;
			int colDiff = (s->colors[0][idx] != s->colors[1][idx]);
			int charDiff = cdFastBufCmp(s, idx);
			// int charDiff = (strcmp(s->buf[0][idx],
			// 			s->buf[1][idx]) != 0);
			if (colDiff || charDiff || s->ForceREDRAW) {
				cdSETCURSOR(x, s->height - y - 1);
				cdSetfgbgColor(s->colors[s->idx][idx]);

				// if (s->showCursor && s->cursorX == x &&
				// s->cursorY == y) {
				// 	if (cdScreenHasTimePassedms(s, 5)) {
				// 	unsigned long long color = 0;
				// 	setfg(&color, 255, 100, 100);
				// 	setfgbgColor(color);
				// 	}
				// }
				printf("%s", s->buf[s->idx][idx]);
				if (s->colors[s->idx][idx] != cdDefault) {
					printf(cdReset);
				}
			} // end of conditional draw
		}
		// printf("\n");
	}
	if (s->showCursor) {
		cdSETCURSOR(s->cursorX, s->height - 1 - s->cursorY);
	}
	cdSETCURSOR(0, s->height - 1);
	printf("\n");
	s->ForceREDRAW = 0;
	s->counter++;
}

unsigned long long cdScreenGetCounter(cdScreen *s)
{
	return s->counter;
}

int cdScreenIsFrameMod(cdScreen *s, int n)
{
	return (s->counter % n) == 0;
}

int cdTimePassedms(cdScreen *s, int n)
{
	// we do max in case the division gives zero (rounding down) as we need
	// to wait at least one cycle
	int numCounters = cdMAX((n * 1000) / s->avgFrameTime, 1);
	return (s->counter % numCounters) == 0;
}

char *cdScreenGet(cdScreen *s, int x, int y)
{
	if (!CHECKWITHIN(s, x, y)) {
		return "\0";
	}
	return s->buf[s->idx][y * s->width + x];
}

void cdScreenSet(cdScreen *s, int x, int y, char *c, cdColor_t color)
{
	if (c[0] == '\0' || !CHECKWITHIN(s, x, y)) {
		return;
	}
	int idx = y * s->width + x;
	// printf("x: %d, y: %d, idx: %d\n", x, y, idx);
	strcpy(s->buf[s->idx][idx], c);
	s->colors[s->idx][idx] = color;
	s->status[idx] = SCREENWRITTEN;
}

void cdScreenSetBox(cdScreen *s, cdBox b, char *symbols, cdColor_t color)
{
	int symb = 0;
	for (int y = b.y; y < b.y+b.h; y++) {
		for (int x = b.x; x < b.x + b.w; x++) {
			char c = symbols[symb];
			if (c == '\0') {
				return;
			}
			cdScreenSetChar(s, x, y, c);
			cdScreenSetColor(s, x, y, color);
			symb++;
		}
	}
}

void cdScreenSetChar(cdScreen *s, int x, int y, char c)
{
	if (c == '\0' || !CHECKWITHIN(s, x, y) || c == '\n') {
		return;
	}
	int idx = y * s->width + x;
#ifdef cdDEBUG
	if (s->status[idx] == SCREENWRITTEN) {
		fprintf(s->logFile,"SET CHAR ALREADY SET "
				" (%d, %d): old %c, new %c\n", x, y,
				s->buf[s->idx][idx][0], c);
	}
#endif // cdDEBUG

	s->buf[s->idx][idx][0] = c;
	s->buf[s->idx][idx][1] = '\0';
	s->status[idx] = SCREENWRITTEN;
}

void cdScreenSetStringBox(cdScreen *s, cdBox box, char *str, cdColor_t color)
{
	if (!calcStringBoxSize(s, &box, str)) {
		printf("Error calculating stringbox size\n");
		return;
	}
	cdScreenBorder(s, box, "Text", color);
	// increment as we take the border into account now
	box.x++;
	box.y--;
	cdScreenSetString(s, box, str, color);
}


void cdScreenNewFrame(cdScreen *s)
{
	s->frameStartTime = cdGetTimeNow();
	int newW, newH;
	struct winsize cdWinSize; // from ioctl
	ioctl(0, TIOCGWINSZ, &cdWinSize);
	newW = cdWinSize.ws_col - 0;
	newH = cdWinSize.ws_row - 1;

#ifdef cdDEBUG
	newW = cdWinSize.ws_col - 1; // debug
	newH = cdWinSize.ws_row-1;
#endif
	if (s->width != newW || s->height != newH) {
		// printf("Resizing REALLOC\n");
		s->width = newW;
		s->height = newH;
		const int dim = s->width * s->height;
		s->status = realloc(s->status, dim *
				sizeof(*s->status));
		for (int i = 0; i < 2; i++) { // for each screen buffer
			s->buf[i] = realloc(s->buf[i], dim *
					sizeof(*s->buf[i]));
			s->colors[i] = realloc(s->colors[i], dim *
					sizeof(*s->colors[i]));
			if (s->buf[i] == NULL || s->colors[i] == NULL
					|| s->status == NULL) {
				printf("%s\n", "Error malloc screen->buf, "
					"screen->colors or screen->status");
				exit(1);
			}
		}
		// cdScreenClear(s);
		// cdScreenSwapFrameBuffer(s);
		cdScreenClear(s);
		cdCLEAR();
		s->ForceREDRAW = 1;
		cdScreenPrint(s);
		s->ForceREDRAW = 0;
	}
	cdScreenSwapFrameBuffer(s);
	cdScreenClear(s);
#ifdef cdDEBUG
	s->logFile = fopen("screen.log", "w");
	if (!s->logFile) {
		printf("Error opening screen log file\n");
		exit(2);
	}
	fprintf(s->logFile, "Screen dimensions w x h (%d, %d)\n",
			s->width, s->height);
#endif // cdDEBUG
}

struct timespec cdDiffTime(struct timespec t1, struct timespec t2)
{
	struct timespec res;
	res.tv_sec = t1.tv_sec - t2.tv_sec;
	res.tv_nsec = t1.tv_nsec - t2.tv_nsec;
	return res;
}

float cdDiffTimef(struct timespec t1, struct timespec t2)
{
	float seconds = (t1.tv_sec - t2.tv_sec) * 1e9;
	float ns = t1.tv_nsec - t2.tv_nsec;
	return seconds + ns;
}

unsigned long long cdTimespecToInt(struct timespec t1)
{
	return t1.tv_sec * CDNANOSECONDS + t1.tv_nsec;
}

void cdScreenRender(cdScreen *s, float frameRate_in)
{
	cdScreenPrint(s);
	float targetFrameTime = 1e6 / frameRate_in;
	// we add 200 as that is roughly the time it takes to actually do the
	//  sleep on my pc.
	struct timespec timeNow = cdGetTimeNow();
	// frameTimePassed is in microseconds
	s->frameTimePassed = cdDiffTimef(timeNow, s->frameStartTime) / 1e3;
	s->currentFPS = (1e6) / (float)s->frameTimePassed;

	int frameDelta = targetFrameTime - (s->frameTimePassed+80);
	const struct timespec sleepts = {.tv_sec=0, .tv_nsec = frameDelta*1000};
	if (frameDelta > 0) {
		nanosleep(&sleepts, NULL);
	}

	// THESE NEED TO BE CALCULATED AGAIN AFTER THE SLEEP OTHERWISE THE
	// VALUES WILL READ THE PRE-SLEEP VALUES WHICH ARE WRONG
	timeNow = cdGetTimeNow();
	s->frameTimePassed = cdDiffTimef(timeNow, s->frameStartTime)/1e3;
	s->currentFPS = (1e6) / (float)s->frameTimePassed;

	s->runTime = cdDiffTime(timeNow, s->startTime);
	// unsigned long long nsVal = s->runTime.tv_nsec + timeDiff.tv_nsec;
	// if (nsVal / CDNANOSECONDS >= 1) { // 1e9
	// 	s->runTime.tv_sec += nsVal / CDNANOSECONDS;
	// 	s->runTime.tv_nsec += nsVal % CDNANOSECONDS;
	// } else {
	// 	s->runTime.tv_nsec += nsVal;
	// }

	static int framePos = 0;
	s->frameTimes[framePos] = s->frameTimePassed;
	framePos++;
	if (framePos >= cdNUMFRAMEHISTORY) {
		framePos = 0;
	}
	s->avgFrameTime = 0;
	for (int i = 0; i < cdNUMFRAMEHISTORY; i++) {
		s->avgFrameTime += s->frameTimes[i];
	}
	s->avgFrameTime /= cdNUMFRAMEHISTORY;

	s->maxFrameTime = cdMAX(s->maxFrameTime, s->avgFrameTime);
	if (cdScreenIsFrameMod(s, 1e5)) {
		s->maxFrameTime = s->avgFrameTime;
	}
#ifdef cdDEBUG
	fflush(s->logFile);
	fclose(s->logFile);
#endif // cdDEBUG
}

void cdAddIntBox(cdScreen *s, char *title, long long value, cdColor_t color)
{
	char buf[128];
	sprintf(buf, "%lld", (long long)value);
	cdAddStringBox(s, title, buf, color);
}

void cdAddHexBox(cdScreen *s, char *title, unsigned long long value,
		cdColor_t color)
{
	char buf[128];
	sprintf(buf, "0x%llx", value);
	cdAddStringBox(s, title, buf, color);
}

void cdAddFloatBox(cdScreen *s, char *title, float value, cdColor_t color)
{
	char buf[128];
	sprintf(buf, "%-7g", value);
	cdAddStringBox(s, title, buf, color);
}

// calculates the size of the box, then positions it somewhere on the screen
void cdAddStringBox(cdScreen *s, char *title, char *str, cdColor_t color)
{
	// in case the title is larger than the string, for some reason
	int tlen = strlen(title);
	int slen = strlen(str);
	char *stringToFit = str;
	if (tlen > slen) {
		stringToFit = title;
	}
	cdBox box;
	if (!calcStringBoxSize(s, &box, stringToFit)) {
		printf("Error calculating stringbox size\n");
		return;
	}

	// increment for the border
	box.w += 2;
	box.h += 2;
	if(!cdFreeCoords(s, &box)) {
		return;
	}

	// box.w += 2;
	// box.h += 2;
	cdScreenBorder(s, box, title, color);
	// printf("string: %s\n", str);
	// increment for border
	box.x++;
	box.y+=box.h-2;
	cdScreenSetString(s, box, str, color);
}

int cdGetStringLineNumber(char *str, int width)
{
	// find out how many lines the string will split over
	int spaceInLine = width;
	int len = strlen(str);
	len -= spaceInLine;
	int numLinesNeeded = 1;
	while (len > 0) {
		numLinesNeeded++;
		len -= spaceInLine;
	}
	return numLinesNeeded + 1;
}

// Takes an array which it writes the offset positions of each line end of the
//  string, the string and the maximum width of the space we are allowed to
//  fill
int setStringEndLinePositions(int *EOL, int *numLines, int lineWidth,
		int *w, int *h, char *str)
{
	const int len = strlen(str);
	int numLineEnds = 0;
	int maxWidth = lineWidth;
	if (EOL) {
		memset(EOL, 0, *numLines * sizeof(*EOL));
	}
	for (int i = 0; i < len; i++) {
		// jump to position at end of line, so check i = i+spaceInLine,
		// if not a space then walk backwards in i until we find a
		// whitespace char, that becomes the new i to check +
		// spaceInLine to
		int j = 0;
		for (j = 0; j <= lineWidth; j++) {
			i += 1;
			// printf("i = (i+=1) = %d\n", i);
			if ( i >= len ) {
				goto END;
			}
			if (str[i] == '\n') {
				// printf("got newline in str\n");
				if (EOL && (numLineEnds < *numLines)) {
					// printf("Error: Trying to write too"
					// 		" many lines!\n");
					EOL[numLineEnds++] = i;
				}
				maxWidth = cdMIN(i, maxWidth);
				i++;
				break;
			}
		}

		if (j == lineWidth) {
			// when we've hit the end of the line start looking
			// backwards
			for (; i > i - j; i--) {
				if (str[i] == ' ' || str[i] == '\n') {
					if (EOL && (numLineEnds < *numLines)) {
						// printf("Error: Trying to "
						// "write too many lines!\n");
						EOL[numLineEnds++] = i;
						j = 0;
					}
					maxWidth = cdMAX(maxWidth, i);
					i++;
					break;
				}
			}
		}
	}
END:
	*numLines = numLineEnds;
	*w = maxWidth;
	*h = numLineEnds+1;
	return 1;
}

// add function which takes a bounding box and a string and works out how to
// split it nicely, like how many lines it requires and only split newlines on
// spaces etc not in the middle of words
int calcStringBoxSize(cdScreen *s, cdBox *box, char *str)
{
	const int len = strlen(str);
	int lineWidth = (len < s->width - 4) ? len : s->width -4;

	int numLines = 64;
	int LineEndPos[numLines];
	setStringEndLinePositions(LineEndPos, &numLines,
				lineWidth, &box->w, &box->h, str);
	return 1;
}

// add function which takes a bounding box and a string and works out how to
// split it nicely, like how many lines it requires and only split newlines on
// spaces etc not in the middle of words
int cdScreenSetString(cdScreen *s, cdBox box, char *str, cdColor_t color)
{
	int xpos = box.x;
	int ypos = box.y;
	int width = box.w;
	if ( xpos < 0 ) {
		xpos = s->width + xpos;
	}
	if ( ypos < 0 ) {
		ypos = s->height + ypos;
	}

	const int len = strlen(str);
	int lineWidth = (len < s->width - 4) ? len : s->width -4;
	int numLines = 64;
	int LineEndPos[numLines];
	setStringEndLinePositions(LineEndPos, &numLines,
				lineWidth, &box.w, &box.h, str);


	// const int MaxLines = ypos - numLines;
	const int LineEndXpos = xpos + width + 1;
	// find out how many lines the string will split over
	// ypos += numLines;

	int i = 0;
	for (int y = ypos; y >= ypos - numLines; y--) {
		for (int x = xpos; x < LineEndXpos; x++) {
			for (int j = 0; j < numLines-1; j++) {
				if (LineEndPos[j] == i) {
					i++;
					y--;
					x = xpos;
					break;
				}
			}

			if ( i >= len ) {
				return 0;
			}

			cdScreenSetChar(s, x, y, str[i]);
			cdScreenSetColor(s, x, y, color);
			cdScreenSetStatus(s, x, y, SCREENWRITTEN);
			i++;
		}
	}
	return numLines + 1;
}

void cdScreenSetColor(cdScreen *s, int x, int y, cdColor_t color)
{
	if (!CHECKWITHIN(s, x, y)) {
		return;
	}
	int idx = y * s->width + x;
	s->colors[s->idx][idx] = color;
	s->status[idx] = SCREENWRITTEN;
}

cdColor_t cdScreenGetColor(cdScreen *s, int x, int y)
{
	if (!CHECKWITHIN(s, x, y)) {
		return cdDefault;
	}
	int idx = y * s->width + x;
	return s->colors[s->idx][idx];
}

void cdScreenSetColorAll(cdScreen *s, cdColor_t color)
{
	int	x, y;
	for (y = 0; y < s->height; y++) {
		for (x = 0; x < s->width; x++) {
			cdScreenSetColor(s, x, y, color);
		}
	}
}

void cdScreenSetAll(cdScreen *s, char *c)
{
	int	x, y;
	for (y = 0; y < s->height; y++) {
		for (x = 0; x < s->width; x++) {
			cdScreenSet(s, x, y, c, cdDefault);
		}
	}
}

void cdScreenSetPattern(cdScreen *s, char *c)
{
	int	y, x, len, i;
	i = 0;
	len = strlen(c);
	// printf("Strlen = %d\n", len);
	for (y = 0; y < s->height; y++) {
		for (x = 0; x < s->width; x++) {
			// set the i-th character and set the next element to
			//  the null terminator
			if (!CHECKWITHIN(s, x, y)) {
				continue;
			}
			s->buf[s->idx][y * s->width + x][0] = c[i];
			s->buf[s->idx][y * s->width + x][1] = '\0';
			// printf("i: %d, &c[i]: %s\n", i, &c[i]);
			i = (i+1 >= len) ? 0 : i+1;
		}
	}
}

#endif // CDIMPLEMENTATION


#endif /* end of include guard SCREEN_H */

/**
 * @author      : oli (oli@$HOSTNAME)
 * @file        : plot
 * @created     : Saturday Jul 27, 2024 22:14:47 BST
 */

#ifndef cdPLOT_H
#define cdPLOT_H

#define cdMAXPLOTPOINTS 256

// Axis / Axes
#define cdMAXPLOTAXISSIZE 8
typedef struct cdAxis {
	int size;
	int numTics, ticPrecision, ticSpacing;
	float min, max;
	char tics[cdMAXPLOTPOINTS][cdSTRSIZE];
	char vals[cdMAXPLOTPOINTS][cdMAXPLOTAXISSIZE];
} cdAxis;

// Plots
//  At the minimum the plot needs to have either a pointer to or a copy of the
//  data values and a screen object to draw to
typedef struct cdPlot {
	char useXdata;
	int dataSize;

	cdAxis xAxis;
	cdAxis yAxis;

	float x[cdMAXPLOTPOINTS];
	float y[cdMAXPLOTPOINTS];

	char title[64];
} cdPlot;

// Plot functions
void cdPlotSet(cdPlot *p, float *pltData, int dataSize);
void cdPlotSetdataSize(cdPlot *p, int dataSize);
void cdPlotPushValue(cdPlot *p, float val, int max);
// evaluates the function f between x=[xMin,xMax] in numPoints
void cdPlotEvaluateFunc(cdPlot *p, float xMin, float xMax, int numPoints,
							float (*f)(float));

void cdPlotDraw(cdScreen *s, cdPlot *plot, cdBox box, cdColor_t color);
void cdPlotDrawBlocks(cdScreen *s, cdPlot *plot, cdBox box, cdColor_t color);

// calculates the size of the box, then positions it somewhere on the screen
void cdAddPlot(cdScreen *s, cdPlot *plot, int h, char *title, cdColor_t color);
void cdPlotTitle(cdPlot *p, char *string);
cdPlot cdPlotNew(float *pltData, int dataSize);

#ifdef CDIMPLEMENTATION // cdPlot implementation

#include <string.h>	// memset
#include <stdio.h>	// sprintf
#include <math.h>	// floor
#include <stdlib.h>	// exit

int numPlaces(int n) {
	int r = 1;
	if (n < 0) {
		r++;
		n = -n;
	}
	while (n > 9) {
		n /= 10;
		r++;
	}
	return r;
}

void cdPlotSetAxes(cdPlot *p, int height)
{
	assert(p->dataSize > 0);
	p->xAxis.size = p->dataSize + 1;
	p->yAxis.size = height-3;

	// printf("sizeof(p->xAxis.tics) = %lx\n", sizeof(p->xAxis.tics));
	// memset(p->xAxis.tics, '\0', sizeof(p->xAxis.tics));
	memset(p->xAxis.vals, '\0', sizeof(p->xAxis.vals));
	// memset(p->yAxis.tics, '\0', sizeof(p->yAxis.tics));
	memset(p->yAxis.vals, '\0', sizeof(p->yAxis.vals));

	// if we have not provided x data, now is the time to fill in 0 to
	// dataSize -1
	if (!p->useXdata) {
		for (int i = 0; i < p->dataSize; i++) {
			p->x[i] = i;
		}
	}
	p->xAxis.min = p->x[0];
	p->xAxis.max = p->x[p->dataSize-1];

	// step 1 is to normalise the max value of the p to the screen height
	p->yAxis.max = -10000000.0f;
	p->yAxis.min = 10000000.0f;
	for (int i = 0; i < p->dataSize; i++) {
		p->yAxis.max = cdMAX(p->y[i], p->yAxis.max);
		p->yAxis.min = cdMIN(p->y[i], p->yAxis.min);
	}

	p->xAxis.ticPrecision = 3;
	p->yAxis.ticPrecision = 3;
	for (int i = 0; i < p->dataSize; i++) {
		char buf[64];
		int blen = 0;
		// x axis
		sprintf(buf, "%g", p->x[i]);
		blen = strlen(buf);
		p->xAxis.ticPrecision = cdMAX(p->xAxis.ticPrecision, blen);
		// y axis
		sprintf(buf, "%g", p->y[i]);
		blen = strlen(buf);
		p->yAxis.ticPrecision = cdMAX(p->yAxis.ticPrecision, blen);
	}

	p->xAxis.ticSpacing = 4;
	p->yAxis.ticSpacing = 2;

	// find x spacing so that we get a nice split of xtics
	for (p->xAxis.ticSpacing = 2; p->xAxis.ticSpacing <= 10;
			p->xAxis.ticSpacing++) {
		int denom = p->xAxis.ticPrecision + p->xAxis.ticSpacing;
		if(p->dataSize % denom == 0) {
			break;
		}
	}

	for (p->yAxis.ticSpacing = 1; p->yAxis.ticSpacing <= 10;
			p->yAxis.ticSpacing++) {
		int denom = 1 + p->yAxis.ticSpacing;
		if((p->yAxis.size-1) % denom == 0) {
			break;
		}
	}

	if (p->yAxis.size <= 5) {
		p->yAxis.ticSpacing = 1;
	}

	// x axis
	// this is not null terminated.
	for (int i = 0; i < p->xAxis.size; i++) {
		strcpy(p->xAxis.tics[i], cdHLine);
	}

	// work out values
	//  assume values are 3 characters long, work out how many 3+1
	//  (space) we can fit in the width, then take the max - min
	//  xvalues and mark them along the splits
	p->xAxis.numTics = (p->xAxis.size /
		(p->xAxis.ticSpacing + p->xAxis.ticPrecision)) + 1;
	const float xticVal = (p->xAxis.max - p->xAxis.min) /
						(float)(p->xAxis.numTics-1);
	for (int i = 0; i < p->xAxis.numTics; i++) {
		int x = i * (p->xAxis.ticSpacing + p->xAxis.ticPrecision);
		const int bufSize = p->xAxis.ticPrecision;
		char xbuf[16]; // +1 for '\0'
		sprintf(xbuf, "%-*g", bufSize,
				p->xAxis.min + i * xticVal);
		if (i == (p->xAxis.numTics - 1)) {
		sprintf(xbuf, "%*g", bufSize,
				p->xAxis.min + i * xticVal);
		}

		// strcpy(p->xAxis.vals[x], xbuf);
		for (int j = 0; j < bufSize; j++) {
			int offset = 0;
			if (i != 0) { offset = -1; }
			if(i == (p->xAxis.numTics - 1)) { offset = -2;}
			p->xAxis.vals[x+j+offset][0] = xbuf[j];
		}
		strcpy(p->xAxis.tics[x], cdHDown);
	}

	// y axis
	p->yAxis.numTics = (p->yAxis.size / (p->yAxis.ticSpacing + 1)) + 1;
	const float yticVal = (p->yAxis.max - p->yAxis.min)
		/ (float)(p->yAxis.numTics-1);

	for (int i = 0; i < p->yAxis.size; i++) {
		strcpy(p->yAxis.tics[i], cdVLine);
	}

	for (int i = 0; i < p->yAxis.numTics; i++) {
		const int bufSize = p->yAxis.ticPrecision;
		char ybuf[16]; // +1 for '\0'
		sprintf(ybuf, "%-*g", p->yAxis.ticPrecision,
				p->yAxis.min + yticVal*i);

		int y = i * (p->yAxis.ticSpacing + 1);
		for (int j = 0; j < bufSize; j++) {
			memcpy(&p->yAxis.vals[y][j], &ybuf[j], 1);
			// p->yAxis.vals[y+j][0] = ybuf[j];
		}
		strcpy(p->yAxis.tics[y], cdLDown);
	}
}

void cdPlotCalcAxesWidth(cdPlot *p, cdBox *box)
{
	assert(p->dataSize > 0);
	// set initial
	box->w = p->dataSize + 3;
	// box->h += 2;
	// then calculate the ytics x offset
	cdPlotSetAxes(p, box->h);
	// add that because we need to shift everything to the right
	box->w += p->yAxis.ticPrecision;
	// +1 because the xaxis text is one line high
	// box->h += 1;
}

void cdPlotDrawAxesAndBorder(cdScreen *s, cdPlot *plot, cdBox *box,
		cdColor_t color)
{
	cdLOG("ye");
	int *xpos = &box->x;
	int *ypos = &box->y;
	if ( *xpos < 0 ) {
		*xpos = s->width + *xpos;
	}
	if ( *ypos < 0 ) {
		*ypos = s->height + *ypos;
	}

	assert(plot->dataSize > 0);

	cdPlotCalcAxesWidth(plot, box);
	cdScreenBorder(s, *box, plot->title, color);

	*xpos = *xpos + 1; // for screen border
	*ypos = *ypos + 1; // for screen border

	for (int i = 0; i <= plot->xAxis.size; i++) {
		int xoff = plot->yAxis.ticPrecision;
		cdScreenSet(s, *xpos + i + xoff, *ypos + 1,
				plot->xAxis.tics[i], color);
		cdScreenSet(s, *xpos + i + xoff, *ypos,
				plot->xAxis.vals[i], color);
	}
	// now that we are finished with the xaxis just add 1 to ypos
	//  so that the rest of the p starts from above the xaxis
	*ypos = *ypos + 1;

	for (int i = 0; i < plot->yAxis.size; i++) {
		cdScreenSet(s, *xpos+plot->yAxis.ticPrecision, *ypos + i,
				plot->yAxis.tics[i], color);

		// cdScreenSet(s, *xpos + i, *ypos, plot->yAxis.vals[i], color);
		for (int j = 0; j < plot->yAxis.ticPrecision; j++) {
			int x = *xpos + j;
			int y = *ypos + i;
			if (plot->yAxis.vals[i][j] == '\0') {
				continue;
			}
			cdScreenSetChar(s, x, y, plot->yAxis.vals[i][j]);
			cdScreenSetColor(s, x, y, color);
		}
	}
	*xpos += plot->yAxis.ticPrecision;

	// set the origin
	cdScreenSet(s, *xpos, *ypos, cdCross, color);
}

// Plot requires a pointer to the data
void cdPlotDraw(cdScreen *s, cdPlot *plot, cdBox box, cdColor_t color)
{
	cdLOGEMPTY();
	cdPlotDrawAxesAndBorder(s, plot, &box, color);
	int xpos = box.x;
	int ypos = box.y;
	int height = box.h - 3;
	// printfVectorf(pltData, dataSize);

	// then when we draw it we divide the values by floor(data[i] / maxVal)
	//  so that they are between 0 and screen->height - 1
	// This just calculates the heights
	assert(plot->dataSize > 0);
	int yVals[plot->dataSize];
	for (int i = 0; i < plot->dataSize; i++){
		float normalised = plot->y[i] - plot->yAxis.min;
		const float yRange = plot->yAxis.max - plot->yAxis.min;
		// float yValHeight = (normalised / yRange) * ((float)height -
		// 1.0f);
		float yValHeight = (normalised / yRange) * (height - 1);

		yVals[i] = floor(yValHeight + 0.5f);
		// printf("plot->y[%d] = %f, maxVal: %f\n",
		// 		i, plot->y[i], yMax);
		// printf("x, y: %d, %d\n", i, height);
	}
	// printfVectori(yVals, plot->dataSize);

	// now actually draw it
	for (int i = 0; i < plot->dataSize; i++) {
		// Depending on the position of the next point work out which
		//  type of curve we need to use.
		const int x = xpos + 1 + i;
		const int y = ypos + yVals[i];
		char *symbol;
		if (i == plot->dataSize - 1) { // last point
			cdScreenSet(s, x, y, cdHLine, color);
			continue;
		}
		int val = yVals[i];
		int next = yVals[i+1];

		if (val == next) {
			symbol = cdHLine;
			cdScreenSet(s, x, y, symbol, color);
		} else if (val < next) {
			cdScreenSet(s, x, y, cdCurveNW, color);
			val++;
			while (val < next) {
				cdScreenSet(s, x, ypos + val,
						cdVLine, color);
				val++;
			}
			cdScreenSet(s, x, ypos + val, cdCurveSE,
					color);
		} else if (val > next) {
			cdScreenSet(s, x, y, cdCurveSW, color);
			val--;
			while (val > next) {
				cdScreenSet(s, x, ypos + val,
						cdVLine, color);
				val--;
			}
			cdScreenSet(s, x, ypos + val, cdCurveNE,
					color);
		}
	}
}

// Plot requires a pointer to the data
void cdPlotDrawBraille(cdScreen *s, cdPlot *plot, cdBox box, cdColor_t color)
{
	cdPlotDrawAxesAndBorder(s, plot, &box, color);
	int xpos = box.x;
	int ypos = box.y;
	int height = box.h;
	// printfVectorf(pltData, dataSize);

	// then when we draw it we divide the values by floor(data[i] / maxVal)
	//  so that they are between 0 and screen->height - 1
	// This just calculates the heights
	int yVals[plot->dataSize];
	for (int i = 0; i < plot->dataSize; i++){
		float normalised = plot->y[i] - plot->yAxis.min;
		const float yRange = plot->yAxis.max - plot->yAxis.min;
		// float yValHeight = (normalised / yRange) * ((float)height -
		// 1.0f);
		float yValHeight = (normalised / yRange) * (4*height - 1);

		yVals[i] = floor(yValHeight + 0.5f);
		// printf("plot->y[%d] = %f, maxVal: %f\n",
		// 		i, plot->y[i], yMax);
		// printf("x, y: %d, %d\n", i, height);
	}
	// printfVectori(yVals, plot->dataSize);

	// now actually draw it
	for (int i = 0; i < plot->dataSize; i++) {
		// Depending on the position of the next point work out which
		//  type of curve we need to use.
		int val = yVals[i];
		const int x = xpos + 1 + i;
		const int y = ypos + val / 4;
		int binPos = val % 4;
		char symbol[8];

		// we have to go in even, odd pairs
		// first get the ypos of the first and second points by
		// getting their yvals and divide by 4 to get ypos
		// then check point 2*i and 2*i+1 are in the same ypos bin

		int idx = 0;
		switch (binPos) {
			case 3:
				idx |= (1 << 0) | (1 << 3);
				// fall through
			case 2:
				idx |= (1 << 1) | (1 << 4);
				// fall through
			case 1:
				idx |= (1 << 2) | (1 << 5);
				// fall through
			case 0:
				idx |= (1 << 6) | (1 << 7);
				break;
		}
		// printf("yVals[%d] = %d, ypos = %d, binPos = %d, idx = %d\n",
				// i, val, ypos, binPos, idx);
		generateBrailleStr(symbol, idx);
		// printf("symbol = %s\n", symbol);
		cdScreenSet(s, x, y, symbol, color);
	}
}

// Plot requires a pointer to the data
void cdPlotDrawBlocks(cdScreen *s, cdPlot *plot, cdBox box, cdColor_t color)
{
	const int yFactor = 2;

	cdPlotDrawAxesAndBorder(s, plot, &box, color);
	int xpos = box.x;
	int ypos = box.y;
	int height = box.h;
	// printfVectorf(pltData, dataSize);

	// then when we draw it we divide the values by floor(data[i] / maxVal)
	//  so that they are between 0 and screen->height - 1
	// This just calculates the heights
	int yVals[plot->dataSize];
	for (int i = 0; i < plot->dataSize; i++){
		float normalised = plot->y[i] - plot->yAxis.min;
		const float yRange = plot->yAxis.max - plot->yAxis.min;
		// float yValHeight = (normalised / yRange) * ((float)height -
		// 1.0f);
		float yValHeight = (normalised / yRange) * (yFactor*height - 1);

		yVals[i] = floor(yValHeight + 0.5f);
		// printf("plot->y[%d] = %f, maxVal: %f\n",
		// 		i, plot->y[i], yMax);
		// printf("x, y: %d, %d\n", i, height);
	}
	// printfVectori(yVals, plot->dataSize);

	// now actually draw it
	for (int i = 0; i < plot->dataSize; i++) {
		// Depending on the position of the next point work out which
		//  type of curve we need to use.
		// const int firstPoint = (i == 0);
		// const int lastPoint = (i == plot->dataSize -1);
		int val = yVals[i];
		const int x = xpos + 1 + i;
		const int y = ypos + val / yFactor;
		int binPos = val % yFactor;

		// we have to go in even, odd pairs
		// first get the ypos of the first and second points by
		// getting their yvals and divide by 4 to get ypos
		// then check point 2*i and 2*i+1 are in the same ypos bin

		char *symbol = "\0";
		switch (binPos) {
			case 0:
				symbol = cdLHalfBlock;
				break;
			case 1:
				symbol = cdUHalfBlock;
				break;
		}
		cdScreenSet(s, x, y, symbol, color);
		// printf("yVals[%d] = %d, ypos = %d, binPos = %d\n",
		// 		i, val, ypos, binPos);
		// printf("symbol = %s\n", symbol);
	}
}

// calculates the size of the box, then positions it somewhere on the screen
void cdAddPlot(cdScreen *s, cdPlot *plot, int h, char *title, cdColor_t color)
{
	cdBox box = {.h = h};
	if (plot->dataSize <= 0) {
		return;
		printf("Error plot->dataSize == 0, with title %s\n", title);
		exit(1);
	}
	cdPlotCalcAxesWidth(plot, &box);
	if(!cdFreeCoords(s, &box)) {
		return;
	}

	cdPlotTitle(plot, title);
	cdPlotDraw(s, plot, box, color);
}

void cdPlotTitle(cdPlot *p, char *string)
{
	strcpy(p->title, string);
}

void cdPlotSetdataSize(cdPlot *p, int dataSize)
{
	if (dataSize >= cdMAXPLOTPOINTS) {
		printf("Error maximum number of points in a plot is %d"
				"you are trying to use a plot with %d points\n",
				cdMAXPLOTPOINTS, dataSize);
		exit(2);
	}
	p->dataSize = dataSize;
	memset(p->x, 0, dataSize * sizeof(p->x[0]));
	memset(p->y, 0, dataSize * sizeof(p->y[0]));
}

// ideally the plot object would have a push value which pops the first value
// and pushes back the most recent to the end
void cdPlotPushValue(cdPlot *p, float val, int max)
{
	if (p->dataSize < max) {
		cdPlotSetdataSize(p, max);
	}
	for (int i = 0; i < p->dataSize - 1; i++) {
		p->y[i] = p->y[i+1];
	}
	p->y[p->dataSize - 1] = val;
}
// evaluates the function f between x=[xMin,xMax] in numPoints
void cdPlotEvaluateFunc(cdPlot *p, float xMin, float xMax, int numPoints,
							float (*f)(float))
{
	cdPlotSetdataSize(p, numPoints);
	const float dx = (xMax - xMin) / (float)(numPoints-1);
	for (int i = 0; i < numPoints; i++) {
		p->x[i] = xMin + i * dx;
		p->y[i] = f(p->x[i]);
	}
	p->useXdata = 1;
}


void cdPlotSet(cdPlot *p, float *pltData, int dataSize)
{
	cdPlotSetdataSize(p, dataSize);
	memcpy(p->y, pltData, dataSize * (sizeof(*pltData)));
	p->useXdata = 0;
	// printfVectorf(p->y, dataSize);
	// printf("plot datasize %d\n",p->dataSize);
}
#endif // CDIMPLEMENTATION
#endif /* end of include guard PLOT_H */


#ifndef CDRAW_GAME_H
#define CDRAW_GAME_H

void cdGameSpaceInv(cdScreen *s, char input);

typedef struct cdEntity {
	int dx, dy;
	cdBox box;
	char symbol;
	int isActive;
} cdEntity;


typedef struct cdGame {
	int init;
	cdBox box;
	cdColor_t color;
} cdGame;

int cdIsBoxOverlap(cdBox b1, cdBox b2);
int cdIsInside(cdBox box, int x, int y);

#ifdef CDIMPLEMENTATION // cdGame implementation

int cdIsInside(cdBox box, int x, int y)
{
	int inX = (box.x <= x && x < (box.x + box.w));
	int inY = (box.y <= y && y < (box.y + box.h));
	return inX && inY;
}


int cdIsBoxOverlap(cdBox b1, cdBox b2)
{
	int x = (b1.x < (b2.x+b2.w)) && (b2.x < (b1.x+b1.w));
	int y = (b1.y < (b2.y+b2.h)) && (b2.y < (b1.y+b1.h));
	return x && y;
}

void cdGameBreakout(cdScreen *s, char input)
{
	static cdGame g = {.box.w = 40, .box.h = 10, .init = 1, .color=cdCyan};

	if (!cdFreeCoords(s, &g.box)) {
		return;
	}
	cdScreenBorder(s, g.box, "Breakout", g.color);

	// Then we offset x,y,w,h to account for the border
	int x = g.box.x + 1;
	int y = g.box.y + 1;
	int w = g.box.w - 2;
	int h = g.box.h - 2;

	// setup block
	static cdEntity ship;
	static cdEntity b;
	const int numEnemies = 32;
	static cdEntity enemies[32];

	if (g.init) {
		ship.box.x = w / 2 - 1;
		ship.box.y = 0;
		ship.box.h = 1;
		ship.box.w = 3;
		ship.symbol = '-';
		ship.isActive = 1;
		ship.dx = 2;
		ship.dy = 0;

		b.box.x = 0;
		b.box.y = 0;
		b.box.w = 1;
		b.box.h = 1;
		b.dx = 1;
		b.dy = 1;
		b.symbol = '.';
		b.isActive = 1;

		for (int i = 0; i < numEnemies; i++) {
			cdEntity *e = &enemies[i];
			int x = 2*i+1;
			int y = 1;

			if (x >= w) {
				x = x - w + 1;
				y++;
			}

			e->box.x = x;
			e->box.y = h-y;
			e->box.w = 1;
			e->box.h = 1;
			e->dx = 0;
			e->dy = 0;
			e->isActive = 1;
			e->symbol = '_';
		}
		g.init = 0;
	}

	switch (input) {
		case 'w':
			if ((ship.box.y + ship.dy) < h) {
				ship.box.y += ship.dy;
			}
			break;
		case 's':
			if ((ship.box.y - ship.dy) >= 0) {
				ship.box.y -= ship.dy;
			}
			break;
		case 'a':
			if ((ship.box.x - ship.dx) >= 0) {
				ship.box.x -= ship.dx;
			}
			break;
		case 'd':
			if ((ship.box.x + ship.dx) < w) {
				ship.box.x += ship.dx;
			}
			break;
		case ' ':
			b.isActive = 1;
			b.box.x = ship.box.x;
			b.box.y = ship.box.y;
			break;
		case 'r':
			g.init = 1;
	}

	if (b.isActive && cdTimePassedms(s, 400)) {
		if (b.box.y < 0 || b.box.y >= h) {
			b.dy = -b.dy;
		}
		if (b.box.x < 0 || b.box.x >= w) {
			b.dx = -b.dx;
		}
		b.box.y += b.dy;
		b.box.x += b.dx;

		if (cdIsBoxOverlap(b.box, ship.box)) {
			b.dy = -b.dy;
			// b.box.y += b.dy;
		}
		for (int i = 0; i < numEnemies; i++) {
			cdEntity *e = &enemies[i];
			if (e->isActive) {
				if (cdIsBoxOverlap(b.box, e->box)) {
					e->isActive = 0;
					b.dy = -b.dy;
				}
				cdScreenSet(s, x + e->box.x, y + e->box.y,
						&e->symbol, g.color);
			}
		}
	}

	if (b.isActive) {
		cdScreenSet(s, x + b.box.x, y + b.box.y, &b.symbol, g.color);
	}
	if (ship.isActive) {
		cdScreenSet(s, x + ship.box.x-1, y + ship.box.y, &ship.symbol,
				g.color);
		cdScreenSet(s, x + ship.box.x, y + ship.box.y, &ship.symbol,
				g.color);
		cdScreenSet(s, x + ship.box.x+1, y + ship.box.y, &ship.symbol,
				g.color);
	}

	for (int i = 0; i < numEnemies; i++) {
		cdEntity *e = &enemies[i];
		if (e->isActive) {
			cdScreenSet(s, x + e->box.x, y + e->box.y, &e->symbol,
					g.color);
		}
	}
}

void cdGameSpaceInv(cdScreen *s, char input)
{
	static cdGame g = {.box.w = 26, .box.h = 10, .init = 1, .color=cdGreen};

	if (!cdFreeCoords(s, &g.box)) {
		return;
	}
	cdScreenBorder(s, g.box, "Space Invaders", g.color);

	// Then we offset x,y,w,h to account for the border
	int x = g.box.x + 1;
	int y = g.box.y + 1;
	int w = g.box.w - 2;
	int h = g.box.h - 2;

	// setup block
	static cdEntity ship;
	static cdEntity b;
	const int numEnemies = 6;
	static cdEntity enemies[6];

	if (g.init) {
		ship.box.x = w / 2 - 1;
		ship.box.y = 0;
		ship.symbol = 'x';
		ship.isActive = 1;
		ship.dx = 2;
		ship.dy = 0;

		b.box.x = 0;
		b.box.y = 0;
		b.box.w = 1;
		b.box.h = 1;
		b.dx = 0;
		b.dy = 1;
		b.symbol = '#';
		b.isActive = 0;

		for (int i = 0; i < numEnemies; i++) {
			cdEntity *e = &enemies[i];
			e->box.x = 4*i;
			e->box.y = h-1;
			e->box.w = 1;
			e->box.h = 1;
			e->dx = 2;
			e->dy = 1;
			e->isActive = 1;
			e->symbol = 'O';
		}
		g.init = 0;
	}

	switch (input) {
		case 'w':
			if ((ship.box.y + ship.dy) < h) {
				ship.box.y += ship.dy;
			}
			break;
		case 's':
			if ((ship.box.y - ship.dy) >= 0) {
				ship.box.y -= ship.dy;
			}
			break;
		case 'a':
			if ((ship.box.x - ship.dx) >= 0) {
				ship.box.x -= ship.dx;
			}
			break;
		case 'd':
			if ((ship.box.x + ship.dx) < w) {
				ship.box.x += ship.dx;
			}
			break;
		case ' ':
			b.isActive = 1;
			b.box.x = ship.box.x;
			b.box.y = ship.box.y;
			break;
		case 'r':
			g.init = 1;
	}


	if (b.isActive && cdTimePassedms(s, 50)) {
		if (b.box.y < h) {
			b.box.y += b.dy;
		} else {
			b.isActive = 0;
		}
	}

	int EntityTime = cdTimePassedms(s, 500);
	if (EntityTime) {
		for (int i = 0; i < numEnemies; i++) {
			cdEntity *e = &enemies[i];
			int next = e->box.x + e->dx;
			if (next >= w || next < 0) {
				e->box.x += e->dx;
				e->dx = -e->dx;
				e->box.y -= e->dy;
			}
			e->box.x += e->dx;
		}
	}

	if (b.isActive) {
		cdScreenSet(s, x + b.box.x, y + b.box.y, &b.symbol, g.color);
	}
	if (ship.isActive) {
		cdScreenSet(s, x + ship.box.x, y + ship.box.y, &ship.symbol,
				g.color);
	}

	for (int i = 0; i < numEnemies; i++) {
		cdEntity *e = &enemies[i];
		if (cdIsBoxOverlap(b.box, e->box)) {
			e->isActive = 0;
		}
		if (e->isActive) {
			cdScreenSet(s, x + e->box.x, y + e->box.y, &e->symbol,
					g.color);
		}
	}
}

#endif // CDIMPLEMENTATION for cdGame implementation
#endif  // CDRAW_GAME_H

#ifndef CDRAW_WINDOW_H // cdWin cdWindow
#define CDRAW_WINDOW_H

typedef struct cdWinEntry {
	int isActive;
	cdBox box;
	cdColor_t color;
	char str[64];
} cdWinEntry;

#define cdMAXWINDOWENTRIES 32
typedef struct cdWin {
	int current;	// index to the last isActive option in the menu
	int cursorX, cursorY;
	cdBox box;	// the bounding box of the window
	// menu options, thing inside the window
	cdWinEntry entries[cdMAXWINDOWENTRIES];
} cdWin;

cdWinEntry *cdWindowAddString(cdScreen *s, cdWin *win, char *str, cdColor_t color);
void cdSetBoxColor(cdScreen *s, cdBox *box, cdColor_t color_in);
void cdSetHightlight(cdScreen *s, cdBox box);
void cdMenu(cdScreen *s, char *input);

#ifdef CDIMPLEMENTATION // cdWindow implementation

cdWinEntry *cdWindowAddString(cdScreen *s, cdWin *win, char *str, cdColor_t color)
{
	cdWinEntry *entry = &win->entries[win->current];
	if (!calcStringBoxSize(s, &entry->box, str)) {
		printf("Error calculating stringbox size\n");
		return entry;
	}

	if (!cdFreeCoordsRegion(s, &entry->box, win->box)) {
		printf("Error\n");
		return entry;
	}

	strcpy(entry->str, str);
	cdScreenSetString(s, entry->box, entry->str, entry->color);
	// entry->isActive = 1;
	win->current++;

	cdLOG(printfcdBox(win->box));
	return entry;
}

void cdSetBoxColor(cdScreen *s, cdBox *box, cdColor_t color_in)
{
	for (int y = box->y; y < box->y + box->h; y++) {
		for (int x = box->x; x < box->x + box->w; x++) {
			char c = cdScreenGet(s, x, y)[0];
			if (c == ' ' || c == '\0') {
				continue;
			}
			// cdColor_t color = cdScreenGetColor(s, x, y);
			// cdAddHexBox(s, "color", color, color);
			cdScreenSetColor(s, x, y, color_in);
		}
	}
}

void cdSetHightlight(cdScreen *s, cdBox box)
{
	for (int y = box.y; y < box.y + box.h; y++) {
		for (int x = box.x; x < box.x + box.w; x++) {
			char c = cdScreenGet(s, x, y)[0];
			if (c == ' ' || c == '\0') {
				continue;
			}
			cdColor_t color = cdScreenGetColor(s, x, y);
			// cdAddHexBox(s, "color", color, color);
			cdSetbgColor(&color, 0x404040);
			cdScreenSetColor(s, x, y, color);
		}
	}
}

void cdMenu(cdScreen *s, char *input)
{
	static cdWin menu;
	menu.box.w = 20;
	menu.box.h = 8;
	menu.current = 0;
	cdColor_t color = cdGreen;

	if (!cdFreeCoords(s, &menu.box)) {
		return;
	}
	cdScreenBorder(s, menu.box, "Menu", color);

	static int boxPos = 0;
	static int Setup = 1;
	if (Setup) {
		menu.current = 0;
		menu.cursorX = 0;
		menu.cursorY = 0;
		for (int i = 0; i < cdMAXWINDOWENTRIES; i++) {
			cdWinEntry *entry = &menu.entries[i];
			entry->isActive = 0;
			entry->color = cdDefault;
			entry->str[0] = '\0';
		}
		Setup = 0;
	}

	cdWinEntry *spaceInv = cdWindowAddString(s, &menu, "SpaceInvaders ", cdDefault);
	cdWinEntry *breakout = cdWindowAddString(s, &menu, "Breakout ", cdDefault);
	cdWinEntry *exit = cdWindowAddString(s, &menu, "Exit ", cdDefault);

	if (input[0] == '\t') {
		boxPos++;
		if (boxPos >= menu.current) { boxPos = 0; }
	}

	for (int i = 0; i < cdMAXWINDOWENTRIES; i++) {
		if (boxPos == i) {
			menu.cursorX = menu.entries[i].box.x;
			menu.cursorY = menu.entries[i].box.y;
		}
	}

	for (int i = 0; i < cdMAXWINDOWENTRIES; i++) {
		cdWinEntry *entry = &menu.entries[i];
		if (cdIsInside(entry->box, menu.cursorX, menu.cursorY)) {
			cdSetHightlight(s, entry->box);
			entry->isActive = 1;
			if (input[0] == '\n') {
				entry->isActive ^= 1;
				entry->color = 0xff0000;
			}
		} else {
			entry->isActive = 0;
			entry->color = cdDefault;
		}
	}

	if (spaceInv->isActive) {
		cdGameSpaceInv(s, input[0]);
	}

	if (exit->isActive) {
		cdWindowAddString(s, &menu, "Exiting!", cdDefault);
	}

	if (breakout->isActive) {
		cdGameBreakout(s, input[0]);
	}
}
#endif // cdWin implementation
#endif // CDRAW_WINDOW_H
